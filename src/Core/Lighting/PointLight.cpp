#include "Core/Lighting/PointLight.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;
using namespace NeroReflex::PBRenderer::Core::Lighting;

PointLight::PointLight(glm::vec3 position, glm::vec3 albedo, glm::float32 intensity) noexcept
	: mPosition(glm::vec4(position, 1.0)),
	mAlbedo(glm::vec4(albedo, intensity)) {

}

glm::vec3 PointLight::getPosition() const noexcept {
	return mPosition;
}

glm::vec3 PointLight::getAlbedo() const noexcept {
	return mAlbedo;
}

glm::float32 PointLight::getIntensity() const noexcept {
	return mAlbedo.a;
}