#include "Core/Lighting/DirectionalLight.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;
using namespace NeroReflex::PBRenderer::Core::Lighting;

DirectionalLight::DirectionalLight(const AABB& interestedVolume, glm::vec3 direction, glm::vec3 albedo, glm::float32 intensity) noexcept
	: mDirection(glm::vec4(glm::normalize(direction), 1.0)),
	mAlbedo(glm::vec4(albedo, intensity)),
	mInterestedVolume(interestedVolume) {

}

glm::vec3 DirectionalLight::getDirection() const noexcept {
	return mDirection;
}

glm::vec3 DirectionalLight::getAlbedo() const noexcept {
	return mAlbedo;
}

glm::float32 DirectionalLight::getIntensity() const noexcept {
	return mAlbedo.a;
}

AABB DirectionalLight::getInterestedVolume() const noexcept {
	return mInterestedVolume;
}