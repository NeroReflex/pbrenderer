#include "Core/AABB.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;

AABB::AABB(glm::vec3 min, glm::vec3 max) noexcept
	: mMin(glm::vec4(min, 1.0f)), mMax(glm::vec4(max, 1.0f)) {}

AABB AABB::join(const AABB& aabb) const noexcept {
	glm::vec3 min = glm::vec3(0, 0, 0), max = glm::vec3(0, 0, 0);

	min.x = std::min(mMin.x, aabb.mMin.x);
	min.y = std::min(mMin.y, aabb.mMin.y);
	min.z = std::min(mMin.z, aabb.mMin.z);
	max.x = std::max(mMax.x, aabb.mMax.x);
	max.y = std::max(mMax.y, aabb.mMax.y);
	max.z = std::max(mMax.z, aabb.mMax.z);

	return AABB(min, max);
}

AABB AABB::transform(const glm::mat4& transformMatrix) const noexcept {
	glm::vec3 dimensions = glm::vec3(mMax.x - mMin.x, mMax.y - mMin.y, mMax.z - mMin.z);

	std::array<glm::vec4, 8> v = {
		transformMatrix * glm::vec4(mMin.x               , mMin.y                 , mMin.z               , 1.0f),
		transformMatrix * glm::vec4(mMin.x               , mMin.y                 , mMin.z + dimensions.z, 1.0f),
		transformMatrix * glm::vec4(mMin.x               , mMin.y + dimensions.y  , mMin.z               , 1.0f),
		transformMatrix * glm::vec4(mMin.x               , mMin.y + dimensions.y  , mMin.z + dimensions.z, 1.0f),
		transformMatrix * glm::vec4(mMin.x + dimensions.x, mMin.y                 , mMin.z               , 1.0f),
		transformMatrix * glm::vec4(mMin.x + dimensions.x, mMin.y                 , mMin.z + dimensions.z, 1.0f),
		transformMatrix * glm::vec4(mMin.x + dimensions.x, mMin.y + dimensions.y  , mMin.z               , 1.0f),
		transformMatrix * glm::vec4(mMin.x + dimensions.x, mMin.y + dimensions.y  , mMin.z + dimensions.z, 1.0f),
	};

	for (auto& vertex : v) {
		vertex = vertex / vertex.w;
	}

	float maxX = v[0].x, maxY = v[0].y, maxZ = v[0].z, minX = v[0].x, minY = v[0].y, minZ = v[0].z;

	for (uint32_t i = 1; i < v.size(); ++i) {
		maxX = std::max(v[i].x, maxX);
		maxY = std::max(v[i].y, maxY);
		maxZ = std::max(v[i].z, maxZ);
		minX = std::min(v[i].x, minX);
		minY = std::min(v[i].y, minY);
		minZ = std::min(v[i].z, minZ);
	}

	return AABB(glm::vec3(minX, minY, minZ), glm::vec3(maxX, maxY, maxZ));
}

glm::vec3 AABB::getMinimumPoint() const noexcept {
	return glm::vec3(mMin.x, mMin.y, mMin.z);
}

glm::vec3 AABB::getMaximumPoint() const noexcept {
	return glm::vec3(mMax.x, mMax.y, mMax.z);
}