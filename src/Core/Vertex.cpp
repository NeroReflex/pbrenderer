#include "Core/Vertex.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;

Vertex::Vertex(
	glm::vec3 position,
	glm::vec3 normal,
	glm::vec2 textures_uv,
	uint32_t materialIndex
) noexcept
	: mPosition(glm::vec4(position, 1.0)),
	mNormal(glm::vec4(normal, 0.0)),
	mTexturesUV(textures_uv),
	mMaterialIndex(materialIndex) {

	static_assert(sizeof(Vertex) == 48, "Wrong std140 layout for Vertex class");
}

bool Vertex::operator==(const Vertex& cmp) const noexcept {
	return memcmp(reinterpret_cast<const void*>(&cmp), reinterpret_cast<const void*>(this), sizeof(Vertex)) == 0;
}

bool Vertex::operator!=(const Vertex& cmp) const noexcept {
	return !operator==(cmp);
}

glm::vec4 Vertex::getPosition() const noexcept {
	return mPosition / mPosition.w;
}

glm::vec4 Vertex::getNormal() const noexcept {
	return mNormal;
}

glm::vec2 Vertex::getTextureCoord() const noexcept {
	return mTexturesUV;
}

void Vertex::setMaterialIndex(uint32_t materialIndex) noexcept {
	mMaterialIndex = materialIndex;
}