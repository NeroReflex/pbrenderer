#include "Core/Material.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;

Material::Material(glm::vec3 diffuseColor) noexcept
	: mDiffuseColor(glm::vec4(diffuseColor, 1.0f)),
	mMaterialFlags(0) {

}

glm::vec3 Material::getDiffuseColor() const noexcept {
	return mDiffuseColor;
}

void Material::setDiffuseTextureIndex(uint32_t index) noexcept {
	mDiffuseTextureIndex = index;

	mMaterialFlags |= MATERIAL_FLAGS_HAS_TEXTURE;
}