#include "Core/Texture.h"

// TODO: it is possible to replace malloc, free, realloc and remove assertions
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Core;

Texture::MipLevel::MipLevel(
	glm::uint width,
	glm::uint height,
	const std::vector<TextureDataType>& data
) noexcept
	: mWidth(width),
	mHeight(height),
	mData(data) {

}

Texture::MipLevel::MipLevel(
	glm::uint width,
	glm::uint height,
	std::vector<TextureDataType>&& data
) noexcept
	: mWidth(width),
	mHeight(height),
	mData(std::move(data)) {

}

Texture::MipLevel& Texture::MipLevel::operator=(const MipLevel& src) noexcept {
	if (&src != this) {
		mWidth = src.mWidth;
		mHeight = src.mHeight;
		mData = src.mData;
	}

	return *this;
}

Texture::Texture(
		glm::uint width,
		glm::uint height,
		const std::vector<TextureDataType>& data,
		std::optional<glm::uint> mipLevels
) noexcept
	: mMipLevels(mipLevels.has_value() ? std::min<glm::uint>(maxMipLevels(width, height), mipLevels.value()) : maxMipLevels(width, height)) {

	mMipLevels[0] = MipLevel(width, height, data);

	for (uint32_t mipLevel = 1; mipLevel < mMipLevels.size(); mipLevel++) {
		glm::uint outWidth = mMipLevels[mipLevel - 1].getWidth(), outHeight = mMipLevels[mipLevel - 1].getHeight();
		outWidth = outWidth == 1 ? 1 : outWidth / 2;
		outHeight = outHeight == 1 ? 1 : outHeight / 2;

		std::vector<TextureDataType> resized(outWidth * outHeight);

#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
		stbir_resize_float(
#else
		stbir_resize_uint8(
#endif
			reinterpret_cast<
#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
				const float*
#else	
				const unsigned char*
#endif
			>(mMipLevels[mipLevel - 1].getData().data()), 
			mMipLevels[mipLevel - 1].getWidth(),
			mMipLevels[mipLevel - 1].getHeight(),
			0,
			reinterpret_cast<
#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
				float*
#else	
				unsigned char*
#endif
			>(resized.data()),
			outWidth,
			outHeight,
			0,
			4
		);

		mMipLevels[mipLevel] = std::move(MipLevel(outWidth, outHeight, std::move(resized)));

	}

}

uint32_t Texture::maxMipLevels(uint32_t w, uint32_t h) noexcept {
	PBRENDERER_DBG_ASSERT((((w & (w - 1)) == 0) && ((h & (h - 1)) == 0)));

	uint32_t levels = 1;

	while ((w > 1) && (h > 1)) {
		w = (w == 1) ? 1 : w >> 1;
		h = (h == 1) ? 1 : h >> 1;
		levels++;
	}

	return levels;
}

uint32_t Texture::countLevels() const noexcept {
	return mMipLevels.size();
}

const Texture::MipLevel& Texture::getMipLevel(uint32_t level) const noexcept {
	return mMipLevels[level];
}

const std::vector<Texture::TextureDataType>& Texture::MipLevel::getData() const noexcept {
	return mData;
}

glm::uint Texture::MipLevel::getWidth() const noexcept {
	return mWidth;
}

glm::uint Texture::MipLevel::getHeight() const noexcept {
	return mHeight;
}
