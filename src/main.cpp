#include "Rendering/RenderingSurface.h"

#include "Loaders/Mesh/MeshLoader.h"
#include "Loaders/Mesh/OBJ.h"

#include "Rendering/Scene.h"

#include "Core/Cameras/SpectatorCamera.h"

#include <filesystem>

static const glm::uint32 defaultWindowWidth = 1024;
static const glm::uint32 defaultWindowHeight = 768;

// Set camera
static const glm::vec3 camStartingPosition = glm::vec3(152.0f, 650.0f, -8.5f);

NeroReflex::PBRenderer::Core::Cameras::SpectatorCamera camera(
	camStartingPosition,
	1.0,
	10000.0f,
	NeroReflex::PBRenderer::Core::Cameras::Camera::HeadDown,
	60.0f, //FoV
	-17.2499943f,
	-0.0299999565f
);

int main(int argc, char** argv) {
	if (argc < 2) {
		std::cerr << "Wrong parameters";

		return EXIT_FAILURE;
	}
	
	const std::string filename = std::string(argv[1]);
	std::string dirpath = filename;
	if (!std::filesystem::exists(filename)) {
		std::cerr << "The specified OBJ file " << filename << " could not be found.";

		return EXIT_FAILURE;
	}
	
	while ((dirpath[dirpath.size() - 1] != '/') && (dirpath[dirpath.size() - 1] != '\\') && (dirpath.size() > 0)) {
		dirpath = dirpath.substr(0, dirpath.size() - 1);
	}

	dirpath += "./";

	std::cout << "Loading file " << filename << ", resources are searched on directory " << dirpath << std::endl;

	NeroReflex::PBRenderer::Loaders::Mesh::MeshLoader loader(std::move(std::make_unique<NeroReflex::PBRenderer::Loaders::Mesh::OBJ>()));

	// default size: 480x360
	const auto window = new NeroReflex::PBRenderer::Rendering::RenderingSurface(defaultWindowWidth, defaultWindowHeight, "Hello World");
	
	// create the scene/level
	auto scene = window->createScene<NeroReflex::PBRenderer::Rendering::Scene>();
	
	// Load the model into the scene
	auto loadResult = loader.load(filename, dirpath);
	scene->loadStaticMesh("a_unique_name <3", *loadResult, { glm::mat4(1.0) });
	loadResult.reset();
	
	int windowWidth, windowHeight;
	glfwGetWindowSize(window->getWindow(), &windowWidth, &windowHeight);
	
	// Set HDR
	NeroReflex::PBRenderer::Core::HDR hdr;
	scene->setHDR(hdr);

	scene->addPointLights(
		{
			NeroReflex::PBRenderer::Core::Lighting::PointLight(
				glm::vec3(-609.631042, 170.224915, 106.746536),
				glm::vec3(1.0, 1.0, 1.0),
				0.75
			),
			NeroReflex::PBRenderer::Core::Lighting::PointLight(
				glm::vec3(-624.558899, 169.539825, -189.802292),
				glm::vec3(1.0, 1.0, 0.9),
				0.3
			),
		}
	);

	scene->addDirectionalLights(
		{
			/*NeroReflex::PBRenderer::Core::Lighting::DirectionalLight(
				glm::vec3(-0.6, 0.98, 0.0),
				glm::vec3(1.0, 1.0, 0.90),
				glm::float32(10.2f)
			),

			NeroReflex::PBRenderer::Core::Lighting::DirectionalLight(
				glm::vec3(0.0, 0.98, 0.6),
				glm::vec3(1.0, 1.0, 0.90),
				glm::float32(10.2f)
			)*/

			NeroReflex::PBRenderer::Core::Lighting::DirectionalLight(
				scene->getAABB(), // select all scene as interested volume
				glm::vec3(0, 0.947768, 0.318959),
				glm::vec3(1.0, 1.0, 1.0),
				glm::float32(10000.2f)
			)
		}

	);

	const auto light_view = glm::lookAt(glm::vec3(-0.6, -1.0, 0.0) * -1.0f, glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0));

	double mouseXPos, mouseYPos;
	glfwGetCursorPos(window->getWindow(), &mouseXPos, &mouseYPos);

	float currentTime = glfwGetTime();
	glm::vec2 mousePosition(mouseXPos, mouseYPos);

	const float moveUnitPerSecond = 275.0f;
	const float mouseSensitivity = 0.015f;

	//glfwSetInputMode(window->getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	PBRENDERER_DBG_ONLY( bool close = false; )

	do {
		const float previouisTime = currentTime;
		currentTime = glfwGetTime();
		float deltaTime = currentTime - previouisTime;

		glfwPollEvents();

		bool moved = false;

		float moveQuantity = moveUnitPerSecond * deltaTime;
		if (glfwGetKey(window->getWindow(), GLFW_KEY_W) == GLFW_PRESS) {
			camera.applyMovement(camera.getOrientation(), moveQuantity);
			moved = true;
		}
		if (glfwGetKey(window->getWindow(), GLFW_KEY_S) == GLFW_PRESS) {
			camera.applyMovement(camera.getOrientation(), -moveQuantity);
			moved = true;
		}
		if (glfwGetKey(window->getWindow(), GLFW_KEY_D) == GLFW_PRESS) {
			camera.applyMovement(glm::normalize(glm::cross(glm::vec3(0, 1, 0), camera.getOrientation())), moveQuantity);
			moved = true;
		}
		if (glfwGetKey(window->getWindow(), GLFW_KEY_A) == GLFW_PRESS) {
			camera.applyMovement(glm::normalize(glm::cross(glm::vec3(0, 1, 0), camera.getOrientation())), -moveQuantity);
			moved = true;
		}

		// Read the new mouse position
		glfwGetCursorPos(window->getWindow(), &mouseXPos, &mouseYPos);
		glm::vec2 newMousePosition = glm::vec2(mouseXPos, mouseYPos);

		glm::vec2 orientationChange = newMousePosition - mousePosition;

		// Update the mouse position
		mousePosition = newMousePosition;

		camera.applyHorizontalRotation(orientationChange.x * mouseSensitivity);
		camera.applyVerticalRotation(-1.0f * orientationChange.y * mouseSensitivity); // -1.0f invert axis

		// Get window size and reset aspect
		glfwGetWindowSize(window->getWindow(), &windowWidth, &windowHeight);
		//camera.setAspect(windowWidth, windowHeight);

		scene->setViewProjectionMatrices(
			// View Matrix
			camera.getViewMatrix(),

			// Projection Matrix
			camera.getProjectionMatrix(windowWidth, windowHeight)
		);

		// Render the scene
		window->nextFrame();

		//glfwSwapBuffers(window->getWindow());
		
	} while (!window->shouldClose() PBRENDERER_DBG_ONLY( && !close )  );

	glm::vec3 normalized_one = glm::normalize(glm::vec3(1.0, 1.0, 1.0));

    return EXIT_SUCCESS;
}
