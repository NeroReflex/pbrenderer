#include "Rendering/Pipeline/FinalRenderingPipeline.h"

#include "shaders/vulkan/static_mesh.vert.spv.h"
#include "shaders/vulkan/static_mesh.frag.spv.h"

#include "shaders/vulkan/deferred.vert.spv.h"
#include "shaders/vulkan/deferred.frag.spv.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Pipeline;

FinalRenderingPipeline::FinalRenderingPipeline(
	Rendering::Scene& parentScene
) noexcept
	: SceneElement(parentScene),
	mDescriptorPools(
		std::array<VulkanFramework::DescriptorPool*, 2>(
			{
				getRendererDevice()->g_Device->createDescriptorPool({
					std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, getRendererDevice()->g_Swapchain->getImagesCount() * uint32_t(1)),
					std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::CombinedImageSampler, getRendererDevice()->g_Swapchain->getImagesCount() * uint32_t(MAX_TEXTURE_IN_ARRAY_COUNT))
				}, getRendererDevice()->g_Swapchain->getImagesCount()),
				getRendererDevice()->g_Device->createDescriptorPool({
					std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, getRendererDevice()->g_Swapchain->getImagesCount()* uint32_t(2)),
					std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::InputAttachment, getRendererDevice()->g_Swapchain->getImagesCount() * uint32_t(3)),
					std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::CombinedImageSampler, getRendererDevice()->g_Swapchain->getImagesCount()* uint32_t(DirectionalLightingPipeline::MaxDirectionalLights)),
				}, getRendererDevice()->g_Swapchain->getImagesCount()),

			}
		)
	),
	mRenderingCommandBuffers(getRendererDevice()->g_CommandPool->createCommandBuffers(getRendererDevice()->g_Swapchain->getImagesCount())),
	mRenderingDepthStencilImages(getRendererDevice()->g_Swapchain->getImagesCount()),
	mRenderingPipelineRenderPass(
		getRendererDevice()->g_Device->createRenderPass(
			std::vector<VkAttachmentDescription>({
				VkAttachmentDescription({ // This is the color buffer attachment to be presented
					.flags = 0,
					.format = getRendererDevice()->g_Swapchain->getFormat(),
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
				}),
				VkAttachmentDescription({ // This is the depth+stencil buffer attachment
					.flags = 0,
					.format = DepthStencilImageFormat,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
				}),
				VkAttachmentDescription({ // This is the vPosition attachment
					.flags = 0,
					.format = ColorAttachmentImageFormat,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = VK_IMAGE_LAYOUT_GENERAL
				}),
				VkAttachmentDescription({ // This is the vNormal attachment
					.flags = 0,
					.format = ColorAttachmentImageFormat,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = VK_IMAGE_LAYOUT_GENERAL
				}),
				VkAttachmentDescription({ // This is TextureCoord attachment
					.flags = 0,
					.format = ColorAttachmentImageFormat,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = VK_IMAGE_LAYOUT_GENERAL
				})
				}),
			{
				VulkanFramework::RenderPass::RenderSubPass({}, {2, 3, 4}, 1),
				VulkanFramework::RenderPass::RenderSubPass({2, 3, 4}, {0})
			},
			{
				VkSubpassDependency({
					.srcSubpass = 0,
					.dstSubpass = 1,
					.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
					.dependencyFlags = 0,
				})
			}
		)
	),
	mRenderingPipeline(
		getRendererDevice()->g_Device->createGraphicPipeline(
			mRenderingPipelineRenderPass, 0,
			{
				getRendererDevice()->g_Device->loadVertexShader(
					VulkanFramework::ShaderLayoutBinding(
						{},
						{
							VulkanFramework::ShaderLayoutBinding::PushConstantDescriptor(0, sizeof(glm::mat4) * 2, { VulkanFramework::Shaders::ShaderType::Vertex })
						}
					),
					reinterpret_cast<const char*>(static_mesh_vertVK),
					static_mesh_vertVK_size
				),
				getRendererDevice()->g_Device->loadFragmentShader(VulkanFramework::ShaderLayoutBinding({
					VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::StorageBuffer, 0, 1),
					VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::CombinedImageSampler, 1, MAX_TEXTURE_IN_ARRAY_COUNT)
				}), reinterpret_cast<const char*>(static_mesh_fragVK), static_mesh_fragVK_size)
			},
			std::move(VulkanFramework::Utils::Rasterizer(
				VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT,
				VkFrontFace::VK_FRONT_FACE_CLOCKWISE
			)),
			std::move(VulkanFramework::Utils::DepthStencilConfiguration(
				true,
				true,
				VkCompareOp::VK_COMPARE_OP_LESS,
				true
			)),
			getRendererDevice()->g_Swapchain->getSurfaceDimensions(),
			{
				VulkanFramework::VertexInputBinding(sizeof(Core::Vertex), VkVertexInputRate::VK_VERTEX_INPUT_RATE_VERTEX, {
					VulkanFramework::VertexInputAttribute(0, Core::Vertex::offsetOfPosition(), VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(1, Core::Vertex::offsetOfNormal(), VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(2, Core::Vertex::offsetOfTexturesUV(), VulkanFramework::VertexInputAttribute::AttributeType::vec2Type),
					VulkanFramework::VertexInputAttribute(3, Core::Vertex::offsetOfMaterialIndex(), VulkanFramework::VertexInputAttribute::AttributeType::uintType),
				}),

				VulkanFramework::VertexInputBinding(sizeof(glm::mat4), VkVertexInputRate::VK_VERTEX_INPUT_RATE_INSTANCE, {
					VulkanFramework::VertexInputAttribute(4, 0, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(5, 16, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(6, 32, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(7, 48, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
				})
			}
		)
	),
	mDeferredPipeline(
		getRendererDevice()->g_Device->createGraphicPipeline(
			mRenderingPipelineRenderPass, 1,
			{
				getRendererDevice()->g_Device->loadVertexShader(
					VulkanFramework::ShaderLayoutBinding(),
					reinterpret_cast<const char*>(deferred_vertVK),
					deferred_vertVK_size
				),
				getRendererDevice()->g_Device->loadFragmentShader(VulkanFramework::ShaderLayoutBinding(
					{
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::InputAttachment, 0, 1),
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::InputAttachment, 1, 1),
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::InputAttachment, 2, 1),
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, 3, 1),
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, 4, 1),
						VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::CombinedImageSampler, 5, DirectionalLightingPipeline::MaxDirectionalLights)
					},
					{
						VulkanFramework::ShaderLayoutBinding::PushConstantDescriptor(0, sizeof(pushConstantData), { VulkanFramework::Shaders::ShaderType::Fragment })
					}
				), reinterpret_cast<const char*>(deferred_fragVK), deferred_fragVK_size)
			},
			std::move(VulkanFramework::Utils::Rasterizer(
				VK_CULL_MODE_NONE
			)),
			std::move(VulkanFramework::Utils::DepthStencilConfiguration()),
			getRendererDevice()->g_Swapchain->getSurfaceDimensions(),
			{}
		)
	)
{
	const auto swapchainSurfaceDimensions = getRendererDevice()->g_Swapchain->getSurfaceDimensions();
	const auto swapchainImages = getRendererDevice()->g_Swapchain->getImagesCount();

	for (uint32_t i = 0; i < swapchainImages; ++i) {
		mHDRBuffer.push_back(
			getRendererDevice()->g_Device->createBuffer(
				{ getRendererDevice()->g_GraphicsQueueFamily },
				static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT),
				sizeof(Core::HDR)
			)
		);

		mRenderingFences.push_back(getRendererDevice()->g_Device->createFence(true));

		mImageAvailableSemaphores.push_back(getRendererDevice()->g_Device->createSemaphore());
		mImageRenderedSemaphores.push_back(getRendererDevice()->g_Device->createSemaphore());

		mRenderingCommandBuffers.push_back(getRendererDevice()->g_CommandPool->createCommandBuffer());

		mRenderingDepthStencilImages[i] = getRendererDevice()->g_Device->createImage(
			{ getRendererDevice()->g_GraphicsQueueFamily },
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
			VulkanFramework::Image::ImageType::Image2D,
			swapchainSurfaceDimensions.getWidth(),
			swapchainSurfaceDimensions.getHeight(),
			1,
			DepthStencilImageFormat
		);

		mRenderingDescriptorSets.push_back(
			{
				mDescriptorPools[0]->allocateDescriptorSet(mRenderingPipeline),
				mDescriptorPools[1]->allocateDescriptorSet(mDeferredPipeline)
			}
		);
		
		mRenderingGBuffer.push_back(
			std::array<VulkanFramework::Image*, 3>(
				{
					getRendererDevice()->g_Device->createImage(
						{ getRendererDevice()->g_GraphicsQueueFamily },
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
						VulkanFramework::Image::ImageType::Image2D,
						swapchainSurfaceDimensions.getWidth(),
						swapchainSurfaceDimensions.getHeight(),
						1,
						ColorAttachmentImageFormat
					),
					getRendererDevice()->g_Device->createImage(
						{ getRendererDevice()->g_GraphicsQueueFamily },
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
						VulkanFramework::Image::ImageType::Image2D,
						swapchainSurfaceDimensions.getWidth(),
						swapchainSurfaceDimensions.getHeight(),
						1,
						ColorAttachmentImageFormat
					),
					getRendererDevice()->g_Device->createImage(
						{ getRendererDevice()->g_GraphicsQueueFamily },
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
						VulkanFramework::Image::ImageType::Image2D,
						swapchainSurfaceDimensions.getWidth(),
						swapchainSurfaceDimensions.getHeight(),
						1,
						ColorAttachmentImageFormat
					)
				}
			)
		);
	}
	
	// Allocate every resource needing memory
	for (uint32_t i = 0; i < getRendererDevice()->g_Swapchain->getImagesCount(); ++i) {
		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mRenderingDepthStencilImages[i]);
		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mRenderingGBuffer[i][0]);
		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mRenderingGBuffer[i][1]);
		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mRenderingGBuffer[i][2]);
		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mHDRBuffer[i]);
	}

	for (uint32_t i = 0; i < swapchainImages; ++i) {
		mRenderingGBufferImageViews.push_back(
			std::array<VulkanFramework::ImageView*, 3>(
				{
					mRenderingGBuffer[i][0]->createImageView(
						VulkanFramework::ImageView::ViewType::Image2D,
						mRenderingGBuffer[i][0]->getFormat(),
						static_cast<VkImageAspectFlagBits>(VK_IMAGE_ASPECT_COLOR_BIT)
					),
					mRenderingGBuffer[i][1]->createImageView(
						VulkanFramework::ImageView::ViewType::Image2D,
						mRenderingGBuffer[i][1]->getFormat(),
						static_cast<VkImageAspectFlagBits>(VK_IMAGE_ASPECT_COLOR_BIT)
					),
					mRenderingGBuffer[i][2]->createImageView(
						VulkanFramework::ImageView::ViewType::Image2D,
						mRenderingGBuffer[i][2]->getFormat(),
						static_cast<VkImageAspectFlagBits>(VK_IMAGE_ASPECT_COLOR_BIT)
					)
				}
			)
		);

		// Retrieve the framebuffer
		mRenderingFramebuffers.push_back(
			getRendererDevice()->g_Device->createFramebuffer(
				mRenderingPipelineRenderPass,
				swapchainSurfaceDimensions,
				{
					getRendererDevice()->g_Swapchain->getSwapchainImageByIndex(i)->getImageView(),
					mRenderingDepthStencilImages[i]->createImageView(
						VulkanFramework::ImageView::ViewType::Image2D,
						mRenderingDepthStencilImages[i]->getFormat(),
						static_cast<VkImageAspectFlagBits>(VK_IMAGE_ASPECT_DEPTH_BIT /*| VK_IMAGE_ASPECT_STENCIL_BIT*/)
					),
					mRenderingGBufferImageViews[i][0],
					mRenderingGBufferImageViews[i][1],
					mRenderingGBufferImageViews[i][2],
				}
			)
		);
	}
}

FinalRenderingPipeline::~FinalRenderingPipeline() {
	getRendererDevice()->g_Device->destroy(mRenderingPipelineRenderPass);
	getRendererDevice()->g_Device->destroy(mRenderingPipeline);


	getRendererDevice()->g_Device->destroy(mDescriptorPools[0]);
	getRendererDevice()->g_Device->destroy(mDescriptorPools[1]);
}

void FinalRenderingPipeline::setHDR(const Core::HDR& hdr) noexcept {
	mHDR = hdr;
}

void FinalRenderingPipeline::executeRendering(
	DirectionalLightingPipeline* directionalLightingPipeline,
	glm::mat4 viewMatrix,
	glm::mat4 projectionMatrix,
	const Models::Static::Textures& textures,
	const Models::Static::Materials& materials,
	const std::unordered_map<std::string, std::unique_ptr<Models::Static::ConcreteCollection>>& staticModels
) noexcept {
	// Generate shadow maps from directional lights
	directionalLightingPipeline->generateShadowMaps(staticModels);

	getRendererDevice()->g_Device->waitForFences({ directionalLightingPipeline->getFence() });

	uint32_t i = getRendererDevice()->g_Swapchain->acquireNextImage(mImageAvailableSemaphores[mCurrentFrame]);

	mRenderingFences[mCurrentFrame]->reset();

	// Z-PREPASS PIPELINE
	mRenderingDescriptorSets[mCurrentFrame][0]->bindStorageBuffers(0, { materials.getMaterialsBuffer() });
	mRenderingDescriptorSets[mCurrentFrame][0]->bindCombinedImageSampler(1, textures.getCombinedSamplerTextures(MAX_TEXTURE_IN_ARRAY_COUNT));

	// DEFERRED SHADING PIPELINE
	mRenderingDescriptorSets[mCurrentFrame][1]->bindInputAttachment(0, mRenderingGBufferImageViews[i][0]);
	mRenderingDescriptorSets[mCurrentFrame][1]->bindInputAttachment(1, mRenderingGBufferImageViews[i][1]);
	mRenderingDescriptorSets[mCurrentFrame][1]->bindInputAttachment(2, mRenderingGBufferImageViews[i][2]);
	mRenderingDescriptorSets[mCurrentFrame][1]->bindUniformBuffers(3, { mHDRBuffer[mCurrentFrame] });
	mRenderingDescriptorSets[mCurrentFrame][1]->bindUniformBuffers(4, { directionalLightingPipeline->getDirectionalLightingUniformBuffer() });
	mRenderingDescriptorSets[mCurrentFrame][1]->bindCombinedImageSampler(5, directionalLightingPipeline->getCombinedSamplerShadowmaps());

	std::vector<glm::mat4> matrices({ viewMatrix, projectionMatrix });

	mRenderingCommandBuffers[mCurrentFrame]->registerCommands([this, &i, &directionalLightingPipeline, &matrices, &staticModels](const VkCommandBuffer& commandBuffer)
		{
			// Update the HDR uniform buffer and wait for the completion before executing the fragment shader
			vkCmdUpdateBuffer(commandBuffer, mHDRBuffer[mCurrentFrame]->getNativeBufferHandle(), 0, sizeof(Core::HDR), reinterpret_cast<const void*>(&mHDR));

			VkMemoryBarrier memBarrier = {};
			memBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
			memBarrier.pNext = nullptr;
			memBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			memBarrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;

			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				0,
				1, &memBarrier,
				0, nullptr,
				0, nullptr
			);


			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.pNext = nullptr;
			renderPassInfo.renderPass = mRenderingPipelineRenderPass->getNativeRenderPassHandle();
			renderPassInfo.framebuffer = mRenderingFramebuffers[i]->getNativeFramebufferHandle();
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = {
				getRendererDevice()->g_Swapchain->getSurfaceDimensions().getWidth(),
				getRendererDevice()->g_Swapchain->getSurfaceDimensions().getHeight()
			};

			std::array<VkClearValue, 5> clearColor = {
				VkClearValue({0.0f, 0.0f, 0.0f, 1.0f}),
				VkClearValue({1.0f, 1.0f, 1.0f, 1.0f}),
				VkClearValue({0.0f, 0.0f, 0.0f, 1.0f}),
				VkClearValue({0.0f, 0.0f, 0.0f, 1.0f}),
				VkClearValue({0.0f, 0.0f, 0.0f, 1.0f})
			};
			renderPassInfo.clearValueCount = clearColor.size();
			renderPassInfo.pClearValues = clearColor.data();

			vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mRenderingPipeline->getNativePipelineHandle());

			vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mRenderingPipeline->getNativePipelineLayoutHandle(), 0, 1, &(mRenderingDescriptorSets[mCurrentFrame][0]->getNativeDescriptorSetHandle()), 0, NULL);

			vkCmdPushConstants(commandBuffer, mRenderingPipeline->getNativePipelineLayoutHandle(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4)*matrices.size(), reinterpret_cast<const void*>(matrices.data()));

			for (auto& mesh : staticModels) {
				mesh.second->foreachMesh([this, &commandBuffer](const Models::Static::Instances& instances, const Models::Static::Definition& definition) {
					VkDeviceSize pOffsets[] = { 0, 0 };
					VkBuffer buffers[] = { definition.getVertexBuffer()->getNativeBufferHandle() , instances.getInstanceBuffer()->getNativeBufferHandle() };
					vkCmdBindVertexBuffers(commandBuffer, 0, 2, buffers, pOffsets);

					vkCmdBindIndexBuffer(commandBuffer, definition.getIndexBuffer()->getNativeBufferHandle(), 0, VK_INDEX_TYPE_UINT32);

					vkCmdDrawIndexed(commandBuffer, definition.getVerticesCount(), instances.count(), 0, 0, 0);
					});
			}

			// Switch over to the deferred pipeline
			vkCmdNextSubpass(commandBuffer, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mDeferredPipeline->getNativePipelineHandle());
			vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mDeferredPipeline->getNativePipelineLayoutHandle(), 0, 1, &(mRenderingDescriptorSets[mCurrentFrame][1]->getNativeDescriptorSetHandle()), 0, NULL);
			
			pushConstantData deferredPushConstants(directionalLightingPipeline->getLightsCount());
			vkCmdPushConstants(commandBuffer, mDeferredPipeline->getNativePipelineLayoutHandle(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(pushConstantData), reinterpret_cast<const void*>(&deferredPushConstants));

			vkCmdDraw(commandBuffer, 6, 1, 0, 0);

			vkCmdEndRenderPass(commandBuffer);
		}
	);

	mRenderingCommandBuffers[mCurrentFrame]->submit(
		getRendererDevice()->g_GraphicsQueue,
		mRenderingFences[mCurrentFrame],
		{ mImageRenderedSemaphores[mCurrentFrame] },
		{
			std::tuple(mImageAvailableSemaphores[mCurrentFrame], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT),
			std::tuple(directionalLightingPipeline->getSemaphore(), VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
		} // wait semaphores
	);

	getRendererDevice()->g_Swapchain->presentImageByIndex(getRendererDevice()->g_GraphicsQueue, i, { mImageRenderedSemaphores[mCurrentFrame] });

	mCurrentFrame = i;
}

void FinalRenderingPipeline::waitIdle() const noexcept {
	auto waitFences = std::vector<const VulkanFramework::Fence*>(mRenderingFences.size());

	uint32_t i = 0;
	for (const auto& fence : mRenderingFences)
		waitFences[mCurrentFrame] = fence;

	getRendererDevice()->g_Device->waitForFences(waitFences);
}
