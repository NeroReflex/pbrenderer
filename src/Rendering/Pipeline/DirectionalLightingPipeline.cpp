#include "Rendering/Pipeline/DirectionalLightingPipeline.h"
#include "Rendering/Scene.h"

#include "shaders/vulkan/directional.vert.spv.h"
#include "shaders/vulkan/directional.frag.spv.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Pipeline;

DirectionalLightingPipeline::DirectionalLightingPipeline(
	Rendering::Scene& parentScene,
	uint32_t expOfTwo
) noexcept
	: SceneElement(parentScene),
	mOccupiedDirectionalLights(0),
	mResolutionExpOfTwo(expOfTwo),
	mShadowMapResolution(uint32_t(1) << mResolutionExpOfTwo, uint32_t(1) << mResolutionExpOfTwo),
	mPipelineRenderPass(
		getRendererDevice()->g_Device->createRenderPass(
			std::vector<VkAttachmentDescription>({
				VkAttachmentDescription({ // This is the color buffer attachment to be presented
					.flags = 0,
					.format = ShadowMapImageFormat,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
					.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
					.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
					.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
					.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
					.finalLayout = ShadowmapResultImageLayout
				})
				}),
			std::vector<VulkanFramework::RenderPass::RenderSubPass>{
			VulkanFramework::RenderPass::RenderSubPass({ }, { }, 0)
			}
		)
	),
	mPipeline(
		getRendererDevice()->g_Device->createGraphicPipeline(
			mPipelineRenderPass, 0,
			{
				getRendererDevice()->g_Device->loadVertexShader(
					VulkanFramework::ShaderLayoutBinding(
						{
							VulkanFramework::ShaderLayoutBinding::BindingDescriptor(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, 0, 1),
						},
						{
							VulkanFramework::ShaderLayoutBinding::PushConstantDescriptor(0, sizeof(glm::uint) * 1, { VulkanFramework::Shaders::ShaderType::Vertex })
						}
					),
					reinterpret_cast<const char*>(directional_vertVK),
					directional_vertVK_size
				),
				getRendererDevice()->g_Device->loadFragmentShader(
					VulkanFramework::ShaderLayoutBinding(),
					reinterpret_cast<const char*>(directional_fragVK), directional_fragVK_size
				)
			},
			std::move(VulkanFramework::Utils::Rasterizer(
				/*VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT,
				VkFrontFace::VK_FRONT_FACE_CLOCKWISE*/
			)),
			std::move(VulkanFramework::Utils::DepthStencilConfiguration(
				true,
				true,
				VkCompareOp::VK_COMPARE_OP_LESS,
				true
			)),
			mShadowMapResolution,
			{
				VulkanFramework::VertexInputBinding(sizeof(Core::Vertex), VkVertexInputRate::VK_VERTEX_INPUT_RATE_VERTEX, {
					VulkanFramework::VertexInputAttribute(0, Core::Vertex::offsetOfPosition(), VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(1, Core::Vertex::offsetOfNormal(), VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(2, Core::Vertex::offsetOfTexturesUV(), VulkanFramework::VertexInputAttribute::AttributeType::vec2Type),
					VulkanFramework::VertexInputAttribute(3, Core::Vertex::offsetOfMaterialIndex(), VulkanFramework::VertexInputAttribute::AttributeType::uintType),
				}),

				VulkanFramework::VertexInputBinding(sizeof(glm::mat4), VkVertexInputRate::VK_VERTEX_INPUT_RATE_INSTANCE, {
					VulkanFramework::VertexInputAttribute(4, 0, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(5, 16, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(6, 32, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
					VulkanFramework::VertexInputAttribute(7, 48, VulkanFramework::VertexInputAttribute::AttributeType::vec4Type),
				})
			}
		)
	),
	mDescriptorPool(
		getRendererDevice()->g_Device->createDescriptorPool({
			std::make_tuple(VulkanFramework::ShaderLayoutBinding::BindingType::UniformBuffer, uint32_t(2))
		}, 1)
	),
	mDescriptorSet(mDescriptorPool->allocateDescriptorSet(mPipeline)),
	mShadowMappingResultSampler(
		getRendererDevice()->g_Device->createSampler(
			VulkanFramework::Sampler::Filtering::Nearest, VulkanFramework::Sampler::Filtering::Nearest, VulkanFramework::Sampler::MipmapMode::ModeLinear
		)
	),
	mCommandBuffer(getRendererDevice()->g_CommandPool->createCommandBuffer()),
	mFence(getRendererDevice()->g_Device->createFence(true)),
	mSemaphore(getRendererDevice()->g_Device->createSemaphore()),
	mDirectionalLightsUniformBuffer(
		getRendererDevice()->g_Device->createBuffer(
			{ getRendererDevice()->g_GraphicsQueueFamily },
			static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT),
			DirectionalLightsUniformBufferSize
		)
	)
{

	// remember to allocate space for uniform buffers
	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mDirectionalLightsUniformBuffer);

	for (uint32_t shadowmap = 0; shadowmap < MaxDirectionalLights; ++shadowmap) {
		mShadowMappingResult[shadowmap] = getRendererDevice()->g_Device->createImage(
			{ getRendererDevice()->g_GraphicsQueueFamily },
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VulkanFramework::Image::ImageType::Image2D,
			mShadowMapResolution.getWidth(),
			mShadowMapResolution.getHeight(),
			1,
			ShadowMapImageFormat
		);

		getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mShadowMappingResult[shadowmap]);
	}

	for (uint32_t shadowmap = 0; shadowmap < MaxDirectionalLights; ++shadowmap) {
		mShadowMappingResultView[shadowmap] = mShadowMappingResult[shadowmap]->createImageView(
			VulkanFramework::ImageView::ViewType::Image2D,
			mShadowMappingResult[shadowmap]->getFormat(),
			static_cast<VkImageAspectFlagBits>(VK_IMAGE_ASPECT_DEPTH_BIT /*| VK_IMAGE_ASPECT_STENCIL_BIT*/)
		);

		mFramebuffer[shadowmap] = getRendererDevice()->g_Device->createFramebuffer(
			mPipelineRenderPass,
			mShadowMapResolution,
			{
				mShadowMappingResultView[shadowmap]
			}
		);
	}

	static_assert( (sizeof(std140DirectionalLightData) == 288) && (DirectionalLightsUniformBufferSize <= 65536) && ((sizeof(std140DirectionalLightData) % 16) == 0), "wrong std140 alignment." );
}

DirectionalLightingPipeline::~DirectionalLightingPipeline() {}

const VulkanFramework::Fence* DirectionalLightingPipeline::getFence() const noexcept {
	return mFence;
}

uint32_t DirectionalLightingPipeline::getLightsCount() const noexcept {
	return static_cast<uint32_t>(mOccupiedDirectionalLights);
}

void DirectionalLightingPipeline::addDirectionalLights(const std::vector<Core::Lighting::DirectionalLight>& directionalLights) noexcept {
	PBRENDERER_DBG_ASSERT( ((directionalLights.size() + mOccupiedDirectionalLights) <= MaxDirectionalLights) );

	for (const auto& directionalLight : directionalLights) {
		mDirectionalLights[mOccupiedDirectionalLights++] = std140DirectionalLightData(directionalLight, mResolutionExpOfTwo);
	}

}

void DirectionalLightingPipeline::generateShadowMaps(
	const std::unordered_map<std::string, std::unique_ptr<Models::Static::ConcreteCollection>>& staticModels
) noexcept {

	getRendererDevice()->g_Device->waitForFences({ mFence });
	mFence->reset();

	mDescriptorSet->bindUniformBuffers(0, { mDirectionalLightsUniformBuffer });

	mCommandBuffer->registerCommands([this, &staticModels](const VkCommandBuffer& commandBuffer)
		{
			// Update light uniform buffer
			vkCmdUpdateBuffer(
				commandBuffer,
				mDirectionalLightsUniformBuffer->getNativeBufferHandle(),
				0,
				DirectionalLightsUniformBufferSize,
				reinterpret_cast<const void*>(mDirectionalLights.data())
			);

			VkMemoryBarrier memBarrier = {};
			memBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
			memBarrier.pNext = nullptr;
			memBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			memBarrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
			
			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
				0,
				1, &memBarrier,
				0, nullptr,
				0, nullptr
			);

			uint32_t lightCount = getLightsCount();

			for (uint32_t light = 0; light < MaxDirectionalLights; ++light) {

				std::array<VkClearValue, 5> clearColor = {
					// TODO: this is NOT LEGAL!!! ClearDepth error!!!
					VkClearValue({.depthStencil {
							.depth = SQRT_3 * 2.0f // maximum distance a directional light can travel as everything is done in voxel space
						}
					})
				};

				VkRenderPassBeginInfo renderPassInfo = {};
				renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderPassInfo.pNext = nullptr;
				renderPassInfo.renderPass = mPipelineRenderPass->getNativeRenderPassHandle();
				renderPassInfo.framebuffer = mFramebuffer[light]->getNativeFramebufferHandle();
				renderPassInfo.clearValueCount = clearColor.size();
				renderPassInfo.pClearValues = clearColor.data();
				renderPassInfo.renderArea.offset = { 0, 0 };
				renderPassInfo.renderArea.extent = {
					mShadowMapResolution.getWidth(),
					mShadowMapResolution.getHeight()
				};
				vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline->getNativePipelineHandle());

				vkCmdBindDescriptorSets(
					commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					mPipeline->getNativePipelineLayoutHandle(),
					0,
					1,
					&(mDescriptorSet->getNativeDescriptorSetHandle()),
					0,
					NULL
				);

				// The pipeline is executed for all shadowmaps, but only for the used directional lights draw calls are performed.
				if (light < lightCount) {
					glm::uint lightIndex = light;

					vkCmdPushConstants(
						commandBuffer,
						mPipeline->getNativePipelineLayoutHandle(),
						VK_SHADER_STAGE_VERTEX_BIT,
						0,
						sizeof(glm::uint),
						reinterpret_cast<const void*>(&lightIndex)
					);

					for (auto& mesh : staticModels) {
						mesh.second->foreachMesh([this, &commandBuffer](const Models::Static::Instances& instances, const Models::Static::Definition& definition) {
							VkDeviceSize pOffsets[] = { 0, 0 };
							VkBuffer buffers[] = { definition.getVertexBuffer()->getNativeBufferHandle() , instances.getInstanceBuffer()->getNativeBufferHandle() };
							vkCmdBindVertexBuffers(commandBuffer, 0, 2, buffers, pOffsets);

							vkCmdBindIndexBuffer(commandBuffer, definition.getIndexBuffer()->getNativeBufferHandle(), 0, VK_INDEX_TYPE_UINT32);

							vkCmdDrawIndexed(commandBuffer, definition.getVerticesCount(), instances.count(), 0, 0, 0);
							});
					}
				}

				vkCmdEndRenderPass(commandBuffer);
			}
		}
	);

	mCommandBuffer->submit(getRendererDevice()->g_GraphicsQueue, mFence, { getSemaphore() });
}

const VulkanFramework::Semaphore* DirectionalLightingPipeline::getSemaphore() const noexcept {
	return mSemaphore;
}

const VulkanFramework::Buffer* DirectionalLightingPipeline::getDirectionalLightingUniformBuffer() const noexcept {
	return mDirectionalLightsUniformBuffer;
}

std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> DirectionalLightingPipeline::getCombinedSamplerShadowmaps() const noexcept {
	std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> result(MaxDirectionalLights);
	
	for (uint32_t i = 0; i < MaxDirectionalLights; ++i) {
		result[i] = std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>(
			ShadowmapResultImageLayout,
			mShadowMappingResultView[i],
			mShadowMappingResultSampler
		);
	}

	return result;
}
