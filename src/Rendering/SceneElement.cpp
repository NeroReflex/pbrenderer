#include "Rendering/SceneElement.h"
#include "Rendering/Scene.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;

SceneElement::SceneElement(Rendering::Scene& parentScene) noexcept
	: mScene(parentScene) {}

Scene& SceneElement::getScene() noexcept {
	return mScene;
}

const Scene& SceneElement::getScene() const noexcept {
	return mScene;
}

Device::RendererDevice* SceneElement::getRendererDevice() const noexcept {
	return mScene.mRendererDevice.get();
}
