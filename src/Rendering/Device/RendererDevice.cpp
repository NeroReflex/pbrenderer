#include "Rendering/Device/RendererDevice.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Device;

RendererDevice::RendererDevice(
	GLFWwindow* window,
	std::unique_ptr<VulkanFramework::Instance> instance,
	const VulkanFramework::Utils::SurfaceDimensions& swapchainInitialDimensions
) noexcept
	: g_Instance(std::move(instance)),
	g_Surface(RendererDevice::createSurfaceKHR(g_Instance.get(), window)),
	g_Device(
		g_Instance->openDevice(
			{
				VulkanFramework::QueueFamily::ConcreteQueueFamilyDescriptor(
					{
						VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Transfer,
						VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Compute,
						VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Graphics,
						VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Present
					},
					1
				)
			},
			getRequiredDeviceExtensions(),
			/*g_Surface,*/
			std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>(glfwGetPhysicalDevicePresentationSupport)
		)
	),
	g_GraphicsQueueFamily(g_Device->getQueueFamily(0)),
	g_GraphicsQueue(g_GraphicsQueueFamily->getQueue(0)),
	g_CommandPool(g_Device->createCommandPool({ g_GraphicsQueueFamily })),
	g_Swapchain(g_Device->createSwapchain(g_Surface, { g_GraphicsQueueFamily }, swapchainInitialDimensions.getWidth(), swapchainInitialDimensions.getHeight())),

	// Following parameters are used for copy data CPU <=> GPU
	mStagingBufferSize(stagingBufferSize),
	mStagingBuffer(g_Device->createBuffer({ g_GraphicsQueueFamily }, static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT), mStagingBufferSize)),
	mStagingBufferCommandBuffer(g_CommandPool->createCommandBuffer()),
	mStagingBufferFence(g_Device->createFence()),

	mMemoryPoolHostCoherent(
		g_Device->createMemoryPool(
			static_cast<VkMemoryPropertyFlagBits>(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
			{ mStagingBuffer },
			static_cast<VkDeviceSize>(1024 * 1024 * 32) / static_cast<VkDeviceSize>(Memory::atomicMemoryBlockSize) // 32MB left free in this pool (in addition to the memory required by the staging buffer)
		)
	),
	mMemoryPoolDeviceOnly(
		g_Device->createMemoryPool(
			static_cast<VkMemoryPropertyFlagBits>(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
			(static_cast<VkDeviceSize>(1024 * 1024) / static_cast<VkDeviceSize>(Memory::atomicMemoryBlockSize)) * 1024 * 2, // 2GB left free in this pool
			static_cast<uint32_t>(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		)
	)

{
	// Allocate memory for the staging buffer
	getMemoryPoolHostCoeherent()->malloc(mStagingBuffer);
}

RendererDevice::~RendererDevice() {
	vkDestroySurfaceKHR(g_Instance->getNativeInstanceHandle(), g_Surface, nullptr);
}

VkSurfaceKHR RendererDevice::createSurfaceKHR(VulkanFramework::Instance* instance, GLFWwindow* window) noexcept {
	VkSurfaceKHR surface;

	VK_CHECK_RESULT(glfwCreateWindowSurface(instance->getNativeInstanceHandle(), window, NULL, &surface));

	return surface;
}

VulkanFramework::MemoryPool* RendererDevice::getMemoryPoolDeviceOnly() noexcept {
	return mMemoryPoolDeviceOnly;
}

VulkanFramework::MemoryPool* RendererDevice::getMemoryPoolHostCoeherent() noexcept {
	return mMemoryPoolHostCoherent;
}

void RendererDevice::copyMemoryToBuffer(const void* src, VulkanFramework::Buffer* dst, uint32_t size) const noexcept {
	uint32_t remaining = size;
	uint32_t completedSize = 0;

	while (remaining > 0) {
		const auto currentChunkSize = std::min<uint32_t>(mStagingBuffer->getBufferSize(), remaining);
		void* temp = mStagingBuffer->getAllocationMemoryPool()->mapMemory(mStagingBuffer->getAllocationOffset(), currentChunkSize);
		std::memcpy(temp, reinterpret_cast<const void*>(reinterpret_cast<const char*>(src)+completedSize), currentChunkSize);
		mStagingBuffer->getAllocationMemoryPool()->unmapMemory();

		mStagingBufferCommandBuffer->registerCommands([this, &dst, &currentChunkSize, &completedSize](const VkCommandBuffer& commandBuffer) {
			VkBufferCopy region = {};
			region.srcOffset = 0;
			region.dstOffset = completedSize;
			region.size = currentChunkSize;

			PBRENDERER_DBG_ASSERT((region.size <= (dst->getBufferSize() - region.dstOffset)));

			vkCmdCopyBuffer(commandBuffer, mStagingBuffer->getNativeBufferHandle(), dst->getNativeBufferHandle(), 1, &region);
			});

		mStagingBufferCommandBuffer->submit({ g_GraphicsQueueFamily->getQueue(0) }, mStagingBufferFence);

		// Wait for a previously launched work to finish
		g_Device->waitForFences({ mStagingBufferFence });
		mStagingBufferFence->reset();

		completedSize += currentChunkSize;
		remaining -= currentChunkSize;
	}
}

void RendererDevice::copyBufferToMemory(VulkanFramework::Buffer* const src, void* dst, uint32_t size) const noexcept {
	uint32_t remaining = size;
	uint32_t completedSize = 0;

	while (remaining != 0) {
		const auto currentChunkSize = std::min<uint32_t>(mStagingBufferSize, remaining);

		mStagingBufferCommandBuffer->registerCommands([this, &src, &currentChunkSize, &completedSize](const VkCommandBuffer& commandBuffer) {
			VkBufferCopy region = {};
			region.srcOffset = 0;
			region.dstOffset = completedSize;
			region.size = currentChunkSize;
			vkCmdCopyBuffer(commandBuffer, src->getNativeBufferHandle(), mStagingBuffer->getNativeBufferHandle(), 1, &region);
			});

		mStagingBufferCommandBuffer->submit({ g_GraphicsQueueFamily->getQueue(0) }, mStagingBufferFence);

		// Wait for a previously launched work to finish
		g_Device->waitForFences({ mStagingBufferFence });
		mStagingBufferFence->reset();

		void* temp = mStagingBuffer->getAllocationMemoryPool()->mapMemory(mStagingBuffer->getAllocationOffset(), currentChunkSize);
		std::memcpy(reinterpret_cast<void*>(reinterpret_cast<char*>(dst) + completedSize), temp, currentChunkSize);
		mStagingBuffer->getAllocationMemoryPool()->unmapMemory();

		completedSize += currentChunkSize;
		remaining -= currentChunkSize;
	}
}

void RendererDevice::copyMemoryToImage(
	const void* src,
	VulkanFramework::Image* const dst,
	VkImageLayout dstFinalLayout
) const noexcept {
	uint32_t textelSize = 0;
	switch (dst->getFormat()) {
	case VK_FORMAT_R8G8B8A8_UINT:
	case VK_FORMAT_R8G8B8A8_SINT:
	case VK_FORMAT_R32_SFLOAT:
		textelSize = 4;
		break;
	case VK_FORMAT_R32G32_SFLOAT:
		textelSize = 4*2;
		break;
	case VK_FORMAT_R32G32B32_SFLOAT:
		textelSize = 4 * 3;
		break;
	case VK_FORMAT_R32G32B32A32_SFLOAT:
		textelSize = 4 * 4;
		break;
	default:
		PBRENDERER_DBG_ASSERT(false);
	}

	VkExtent3D extent = dst->getExtent();

	uint32_t texelsCount = extent.width * ((extent.height <= 0) ? 1 : extent.height) * ((extent.depth <= 0) ? 1 : extent.depth);

	const size_t image_requirements_in_bytes = texelsCount * textelSize;

	PBRENDERER_DBG_ASSERT((image_requirements_in_bytes <= mStagingBuffer->getBufferSize()));

	// copy memory from the RAM to the staging buffer
	void* temp = mStagingBuffer->getAllocationMemoryPool()->mapMemory(mStagingBuffer->getAllocationOffset(), mStagingBufferSize);
	std::memcpy(temp, src, image_requirements_in_bytes);
	mStagingBuffer->getAllocationMemoryPool()->unmapMemory();
	
	mStagingBufferCommandBuffer->registerCommands(
		[this, &extent, &dst, &dstFinalLayout](const VkCommandBuffer& commandBuffer) {
			VkBufferImageCopy region = {};
			region.bufferOffset = 0;
			region.bufferRowLength = 0;
			region.bufferImageHeight = 0;
			region.imageExtent = extent;
			region.imageOffset = VkOffset3D({ .x = 0 ,.y = 0 ,.z = 0 });
			
			region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			region.imageSubresource.mipLevel = 0;
			region.imageSubresource.baseArrayLayer = 0;
			region.imageSubresource.layerCount = 1;
			
			VkImageMemoryBarrier topBarrier = {};
			topBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			topBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			topBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			topBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			topBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			topBarrier.image = dst->getNativeImageHandle();
			topBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			topBarrier.subresourceRange.baseMipLevel = 0;
			topBarrier.subresourceRange.levelCount = 1;
			topBarrier.subresourceRange.baseArrayLayer = 0;
			topBarrier.subresourceRange.layerCount = 1;
			topBarrier.srcAccessMask = 0;
			topBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			VkImageMemoryBarrier bottomBarrier = {};
			bottomBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			bottomBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			bottomBarrier.newLayout = dstFinalLayout;
			bottomBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bottomBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bottomBarrier.image = dst->getNativeImageHandle();
			bottomBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			bottomBarrier.subresourceRange.baseMipLevel = 0;
			bottomBarrier.subresourceRange.levelCount = 1;
			bottomBarrier.subresourceRange.baseArrayLayer = 0;
			bottomBarrier.subresourceRange.layerCount = 1;
			bottomBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			bottomBarrier.dstAccessMask = 0;

			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &topBarrier
			);

			vkCmdCopyBufferToImage(commandBuffer, mStagingBuffer->getNativeBufferHandle(), dst->getNativeImageHandle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
		
			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &bottomBarrier
			);
		}
	);

	mStagingBufferCommandBuffer->submit({ g_GraphicsQueueFamily->getQueue(0) }, mStagingBufferFence);

	// Wait for a previously launched work to finish
	g_Device->waitForFences({ mStagingBufferFence });
	mStagingBufferFence->reset();
}

void RendererDevice::copyTextureToImage(const Core::Texture& src, VulkanFramework::Image* const dst, VkImageLayout dstFinalLayout) const noexcept {
	for (uint32_t i = 0; i < src.countLevels(); ++i) {
		copyTextureMipLevelToImage(src, dst, i, dstFinalLayout);
	}
}

void RendererDevice::copyTextureMipLevelToImage(const Core::Texture& src, VulkanFramework::Image* const dst, uint32_t mipLevel, VkImageLayout dstFinalLayout) const noexcept {
	uint32_t textelSize = 0;
	switch (dst->getFormat()) {
	case VK_FORMAT_R8G8B8A8_UINT:
	case VK_FORMAT_R8G8B8A8_SINT:
	case VK_FORMAT_R32_SFLOAT:
		textelSize = 4;
		break;
	case VK_FORMAT_R32G32_SFLOAT:
		textelSize = 4 * 2;
		break;
	case VK_FORMAT_R32G32B32_SFLOAT:
		textelSize = 4 * 3;
		break;
	case VK_FORMAT_R32G32B32A32_SFLOAT:
		textelSize = 4 * 4;
		break;
	default:
		PBRENDERER_DBG_ASSERT(false);
	}

	VkExtent3D extent = VkExtent3D(
		{
			.width = src.getMipLevel(mipLevel).getWidth(),
			.height =src.getMipLevel(mipLevel).getHeight(),
			.depth = 1
		}
	);

	PBRENDERER_DBG_ASSERT( ((extent.width > 0) && (extent.height > 0)) );

	uint32_t texelsCount = extent.width * extent.height;

	const uint32_t image_requirements_in_bytes = texelsCount * textelSize;

	PBRENDERER_DBG_ASSERT((image_requirements_in_bytes <= mStagingBuffer->getBufferSize()));

	// copy memory from the RAM to the staging buffer
	void* temp = mStagingBuffer->getAllocationMemoryPool()->mapMemory(mStagingBuffer->getAllocationOffset(), mStagingBufferSize);
	std::memcpy(temp, src.getMipLevel(mipLevel).getData().data(), image_requirements_in_bytes);
	mStagingBuffer->getAllocationMemoryPool()->unmapMemory();

	mStagingBufferCommandBuffer->registerCommands(
		[this, &extent, &dst, &mipLevel, &dstFinalLayout](const VkCommandBuffer& commandBuffer) {
			VkBufferImageCopy region = {};
			region.bufferOffset = 0;
			region.bufferRowLength = 0;
			region.bufferImageHeight = 0;
			region.imageExtent = extent;
			region.imageOffset = VkOffset3D({ .x = 0 ,.y = 0 ,.z = 0 });

			region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			region.imageSubresource.mipLevel = mipLevel;
			region.imageSubresource.baseArrayLayer = 0;
			region.imageSubresource.layerCount = 1;

			VkImageMemoryBarrier topBarrier = {};
			topBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			topBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			topBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			topBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			topBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			topBarrier.image = dst->getNativeImageHandle();
			topBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			topBarrier.subresourceRange.baseMipLevel = mipLevel;
			topBarrier.subresourceRange.levelCount = 1;
			topBarrier.subresourceRange.baseArrayLayer = 0;
			topBarrier.subresourceRange.layerCount = 1;
			topBarrier.srcAccessMask = 0;
			topBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			VkImageMemoryBarrier bottomBarrier = {};
			bottomBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			bottomBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			bottomBarrier.newLayout = dstFinalLayout;
			bottomBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bottomBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bottomBarrier.image = dst->getNativeImageHandle();
			bottomBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			bottomBarrier.subresourceRange.baseMipLevel = mipLevel;
			bottomBarrier.subresourceRange.levelCount = 1;
			bottomBarrier.subresourceRange.baseArrayLayer = 0;
			bottomBarrier.subresourceRange.layerCount = 1;
			bottomBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			bottomBarrier.dstAccessMask = 0;

			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &topBarrier
			);

			vkCmdCopyBufferToImage(commandBuffer, mStagingBuffer->getNativeBufferHandle(), dst->getNativeImageHandle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &bottomBarrier
			);
		}
	);

	mStagingBufferCommandBuffer->submit({ g_GraphicsQueueFamily->getQueue(0) }, mStagingBufferFence);

	// Wait for a previously launched work to finish
	g_Device->waitForFences({ mStagingBufferFence });
	mStagingBufferFence->reset();
}

std::vector<std::string> RendererDevice::getRequiredInstanceExtensions() {
	std::vector<std::string> instanceExtensions;

	uint32_t glfwExtensionsCount;

	// Query GLFW-required extensions
	const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);
	
	// Import all GLFW extensions to the safe buffer
	for (uint32_t j = 0; j < glfwExtensionsCount; ++j) {
		instanceExtensions.push_back(glfwExtensions[j]);
	}

	return instanceExtensions;
}

std::vector<std::string> RendererDevice::getRequiredDeviceExtensions() {
	std::vector<std::string> deviceExtensions;

	// When creating a device it is important to specify the VK_KHR_swapchain extension or the device cannot be used for rendering
	deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

	// Required to open a conservative rasterization
	/*deviceExtensions.push_back("VK_KHR_get_physical_device_properties2");
	deviceExtensions.push_back("VK_KHR_maintenance2");
	deviceExtensions.push_back("VK_KHR_image_format_list");
	deviceExtensions.push_back("VK_KHR_imageless_framebuffer");*/

	//deviceExtensions.push_back("VK_EXT_conservative_rasterization");

	return deviceExtensions;
}