#include "Rendering/RenderingSurface.h"
#include "Rendering/Scene.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;

static std::unordered_map<uintptr_t, RenderingSurface*> surfacesList = std::unordered_map<uintptr_t, RenderingSurface*>();

void resizeCallback(GLFWwindow* window, int newWidth, int newHeight) {
	RenderingSurface* surface = surfacesList[uintptr_t(window)];
	surface->onResize(newWidth, newHeight);
}

RenderingSurface::RenderingSurface(uint32_t width, uint32_t height, std::string windowTitle) noexcept
	: mWindowTitle(std::move(windowTitle)), mScene(nullptr) {

	if (surfacesList.empty())
		RenderingSurface::initialize();

	mWindow = glfwCreateWindow(width, height, mWindowTitle.c_str(), nullptr, nullptr);

	surfacesList.emplace(uintptr_t(mWindow), this);

	glfwGetWindowSize(mWindow, &(std::get<0>(mWindowDimensions)), &(std::get<1>(mWindowDimensions)));

	glfwSetWindowSizeCallback(mWindow, resizeCallback);
}

RenderingSurface::~RenderingSurface() {
	surfacesList.erase(uintptr_t(mWindow));

	glfwDestroyWindow(mWindow);
	
	mWindow = nullptr;

	if (surfacesList.empty())
		RenderingSurface::terminate();
}

const std::string& RenderingSurface::getTitle() const noexcept {
	return mWindowTitle;
}

bool RenderingSurface::isVulkanSupported() const noexcept {
	return glfwVulkanSupported();
}

uint32_t RenderingSurface::getWidth() const noexcept {
	return static_cast<uint32_t>(std::get<0>(mWindowDimensions));
}

uint32_t RenderingSurface::getHeight() const noexcept {
	return static_cast<uint32_t>(std::get<1>(mWindowDimensions));
}

GLFWwindow* RenderingSurface::getWindow() const noexcept {
	return mWindow;
}

void RenderingSurface::onResize(uint32_t newWidth, uint32_t newHeight) noexcept {
	std::get<0>(mWindowDimensions) = newWidth;
	std::get<1>(mWindowDimensions) = newHeight;

	if (mScene) mScene->onResize(newWidth, newHeight);
}

bool RenderingSurface::initialize() noexcept {
	// Initialize GLFW
	if (glfwInit() == 0) {
		return false;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	return true;
}

void RenderingSurface::terminate() noexcept {
	// Terminate GLFW
	glfwTerminate();
}

void RenderingSurface::nextFrame() const noexcept {
	if (mScene) mScene->onRender();
}

bool RenderingSurface::shouldClose() const noexcept {
	return glfwWindowShouldClose(mWindow);
}

GLFWwindow* RenderingSurface::getWindow() noexcept {
	return mWindow;
}
