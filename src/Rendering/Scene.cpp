#include "Rendering/Scene.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;

Scene::Scene(
	const Rendering::RenderingSurface& window
) noexcept
	: mRenderingSurface(window),
	mRendererDevice(
		std::make_unique<Device::RendererDevice>(
			mRenderingSurface.getWindow(),
			std::move(
				std::make_unique<VulkanFramework::Instance>(
					Device::RendererDevice::getRequiredInstanceExtensions(),
					"PBRenderer",
					window.getTitle(),
					VulkanFramework::Instance::APIVersion::Version_1_2
				)
			),
			VulkanFramework::Utils::SurfaceDimensions(window.getWidth(), window.getHeight())
		)
	),
	mDirectionalLightingPipeline(std::make_unique<Pipeline::DirectionalLightingPipeline>(*this, 11)),
	mFinalRenderingPipeline(std::make_unique<Pipeline::FinalRenderingPipeline>(*this)),
	mStaticMeshesTextures(std::make_unique<Models::Static::Textures>(*this)),
	mStaticMeshesMaterials(std::make_unique<Models::Static::Materials>(*this))
{

}

void Scene::onResize(uint32_t newWidth, uint32_t newHeight) noexcept {
	// TODO: wait for render stop. recreate the swapchain and recreate the graphic pipeline 
}

Core::AABB Scene::getAABB() const noexcept {
	Core::AABB aabb = Core::AABB(glm::vec3(), glm::vec3());
	bool initialized = false;

	for (auto& mesh : mStaticMeshes) {
		aabb = (!initialized) ? mesh.second->getAABB() : aabb.join(mesh.second->getAABB());

		initialized = true;
	}

	return aabb;
}

void Scene::onRender() noexcept {
	mFinalRenderingPipeline->executeRendering(
		mDirectionalLightingPipeline.get(),
		mViewMatrix,
		mProjectionMatrix,
		*mStaticMeshesTextures,
		*mStaticMeshesMaterials,
		mStaticMeshes
	);
}

void Scene::loadStaticMesh(const std::string& name, const Loaders::Mesh::MeshLoader::LoadResult& meshAsLoadResult, std::vector<glm::mat4> instances) noexcept {
	// This will be the base index of a material
	uint32_t materialIndex = mStaticMeshesMaterials->count();

	// load a material for each mesh (at the cost of duplicating some and load them in the materials storage buffer)
	meshAsLoadResult.foreachLoadedMesh(
		[this](const std::string& name, const Loaders::Mesh::MeshLoader::LoadedMesh& loadedMesh) {

			const uintptr_t loadedMaterialSignature = uintptr_t(loadedMesh.getMaterial());

			if (std::find(mLoadedMaterials.cbegin(), mLoadedMaterials.cend(), loadedMaterialSignature) == mLoadedMaterials.cend()) {
				mLoadedMaterials.push_back(loadedMaterialSignature);

				// Create the basic material off from the loaded material
				auto nativeMaterial = loadedMesh.getMaterial()->getNativeMaterial();

				const Core::Texture* diffuseTexture = loadedMesh.getMaterial()->getDiffuseTexture();
				if (diffuseTexture) {
					uintptr_t loadedTextureSignature = uintptr_t(diffuseTexture);

					const auto matereialSearchResult = std::find(mLoadedTextures.cbegin(), mLoadedTextures.cend(), loadedTextureSignature);
					uint32_t textureIndex = std::distance(mLoadedTextures.cbegin(), matereialSearchResult);
					if (textureIndex == mLoadedTextures.size()) {
						mLoadedTextures.emplace_back(loadedTextureSignature);

						mStaticMeshesTextures->addTextures(*diffuseTexture);
					}
					nativeMaterial.setDiffuseTextureIndex(textureIndex);
				}

				mStaticMeshesMaterials->addMaterials({ nativeMaterial });
			}
		}
	);

	// This operation registers all meshes and also update the material index of each vertex buffer
	mStaticMeshes.emplace(name, std::make_unique<Models::Static::ConcreteCollection>(*this, mLoadedMaterials, meshAsLoadResult, instances));
}

const VulkanFramework::Utils::SurfaceDimensions& Scene::getSurfaceDimensions() const noexcept {
	return mRendererDevice->g_Swapchain->getSurfaceDimensions();
}

void Scene::setProjectionMatrix(const glm::mat4& projection) noexcept {
	mProjectionMatrix = projection;
}

void Scene::setViewMatrix(const glm::mat4& view) noexcept {
	mViewMatrix = view;
}

void Scene::setViewProjectionMatrices(const glm::mat4& view, const glm::mat4& projection) noexcept {
	mViewMatrix = view;
	mProjectionMatrix = projection;
}

void Scene::setHDR(const Core::HDR& hdr) const noexcept {
	mFinalRenderingPipeline->setHDR(hdr);
}

void Scene::addPointLights(const std::vector<Core::Lighting::PointLight>& lights) noexcept {
	//TODO: implement point lights

}

void Scene::addDirectionalLights(const std::vector<Core::Lighting::DirectionalLight>& directionalLights) noexcept {
	mDirectionalLightingPipeline->addDirectionalLights(directionalLights);
}
