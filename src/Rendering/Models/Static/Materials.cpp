#include "Rendering/Models/Static/Materials.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Models;
using namespace NeroReflex::PBRenderer::Rendering::Models::Static;

Materials::Materials(
	Rendering::Scene& parentScene,
	const std::vector<Core::Material>& materials
) noexcept
	: SceneElement(parentScene),
	mMaterials(std::min<size_t>(materials.size(), MaxMaterialsNumber)),
	mMaterialsBuffer(
		getRendererDevice()->g_Device->createBuffer(
			{ getRendererDevice()->g_GraphicsQueueFamily },
			static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT),
			sizeof(Core::Material) * MaxMaterialsNumber
		)
	) {

	for (uint32_t i = 0; i < mMaterials.size(); ++i) {
		mMaterials[i] = materials[i];
	}

	// Allocate memory for core buffers on the GPU exclusive memory.
	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mMaterialsBuffer);

	getRendererDevice()->copyMemoryToBuffer(reinterpret_cast<const void*>(mMaterials.data()), mMaterialsBuffer, sizeof(Core::Material)* mMaterials.size());
}

Materials::~Materials() {
	getRendererDevice()->g_Device->destroy(mMaterialsBuffer);
}

uint32_t Materials::count() const noexcept {
	return mMaterials.size();
}

const VulkanFramework::Buffer* Materials::getMaterialsBuffer() const noexcept {
	return mMaterialsBuffer;
}

void Materials::addMaterials(const std::vector<Core::Material>& materials) noexcept {
	uint32_t i = 0;
	while (((materials.size() - i) + mMaterials.size()) && (i < materials.size()) && (mMaterials.size() <= MaxMaterialsNumber)) {
		mMaterials.push_back(materials[i]);

		++i;
	}

	getRendererDevice()->copyMemoryToBuffer(reinterpret_cast<const void*>(mMaterials.data()), mMaterialsBuffer, sizeof(Core::Material)* mMaterials.size());
}