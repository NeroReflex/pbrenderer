#include "Rendering/Models/Static/Definition.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Models;
using namespace NeroReflex::PBRenderer::Rendering::Models::Static;

Definition::Definition(
	Rendering::Scene& parentScene,
	const std::vector<Core::Vertex>& vertexData,
	const std::vector<uint32_t>& vertexIndices,
	const Core::AABB& modelspaceAABB,
	uint32_t materialIndex
) noexcept
	: SceneElement(parentScene),
	mVertexBuffer(getRendererDevice()->g_Device->createBuffer(
		{ getRendererDevice()->g_GraphicsQueueFamily },
		static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT), sizeof(Core::Vertex)*vertexData.size())
	),
	mIndexBuffer(getRendererDevice()->g_Device->createBuffer(
		{ getRendererDevice()->g_GraphicsQueueFamily },
		static_cast<VkBufferUsageFlagBits>(VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT), sizeof(uint32_t)*vertexIndices.size())
	),
	mVerticesCount(vertexIndices.size()),
	mModelspaceAABB(modelspaceAABB) {
	
	// Allocate memory for core buffers on the GPU exclusive memory.
	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mVertexBuffer);
	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mIndexBuffer);

	std::vector<Core::Vertex> verticesWithCorrectedMaterialIndex(vertexData);
	for (auto& vertex : verticesWithCorrectedMaterialIndex)
		vertex.setMaterialIndex(materialIndex);

	getRendererDevice()->copyMemoryToBuffer(reinterpret_cast<const void*>(verticesWithCorrectedMaterialIndex.data()), mVertexBuffer, sizeof(Core::Vertex)* verticesWithCorrectedMaterialIndex.size());
	getRendererDevice()->copyMemoryToBuffer(reinterpret_cast<const void*>(vertexIndices.data()), mIndexBuffer, sizeof(uint32_t)* vertexIndices.size());
}

Definition::~Definition() {}

const VulkanFramework::Buffer* Definition::getVertexBuffer() const noexcept {
	return mVertexBuffer;
}

const VulkanFramework::Buffer* Definition::getIndexBuffer() const noexcept {
	return mIndexBuffer;
}

uint32_t Definition::getVerticesCount() const noexcept {
	return mVerticesCount;
}

const Core::AABB& Definition::getModelspaceAABB() const noexcept {
	return mModelspaceAABB;
}