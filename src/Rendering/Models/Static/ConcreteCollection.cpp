#include "Rendering/Models/Static/ConcreteCollection.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Models;
using namespace NeroReflex::PBRenderer::Rendering::Models::Static;

ConcreteCollection::ConcreteCollection(
	Rendering::Scene& parentScene,
	const std::vector<uintptr_t>& loadedMaterials,
	const Loaders::Mesh::MeshLoader::LoadResult& meshes,
	const std::vector<glm::mat4>& instances
) noexcept
	: SceneElement(parentScene),
	mDefinitions(),
	mInstances(std::make_unique<Instances>(parentScene, instances)) {

	uint32_t i = 0;
	for (const auto& loadedMesh : meshes.getMeshes()) {
		mDefinitions.emplace_back(std::make_unique<Definition>(
			parentScene,
			loadedMesh.second->getVertexData(),
			loadedMesh.second->getIndeces(),
			loadedMesh.second->getAABB(),
			std::distance(loadedMaterials.cbegin(), std::find(loadedMaterials.cbegin(), loadedMaterials.cend(), uintptr_t(loadedMesh.second->getMaterial())))
		));

		++i;
	}
}

ConcreteCollection::~ConcreteCollection() {

}

void ConcreteCollection::foreachMesh(std::function<void(const Instances&, const Definition&)> fn) const noexcept {
	for (const auto& definition: mDefinitions) {
		fn(const_cast<const Instances&>(*mInstances.get()), const_cast<const Definition&>(*definition.get()));
	}
}

Core::AABB ConcreteCollection::getAABB() const noexcept {
	bool initialized = false;
	Core::AABB aabb = Core::AABB(glm::vec3(), glm::vec3());

	foreachMesh([&initialized, &aabb](const Instances& instances, const Definition& definition) {
		const auto modelMatrices = instances.getInstances();
		for (const auto& modelMatrix : modelMatrices) {
			aabb = (!initialized) ? definition.getModelspaceAABB().transform(modelMatrix) : aabb.join(definition.getModelspaceAABB().transform(modelMatrix));

			initialized = true;
		}
	});

	return aabb;
}
