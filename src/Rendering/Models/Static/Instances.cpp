#include "Rendering/Models/Static/Instances.h"
#include "Rendering/Scene.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Models;
using namespace NeroReflex::PBRenderer::Rendering::Models::Static;

Instances::Instances(Rendering::Scene& parentScene, const std::vector<glm::mat4>& instances) noexcept
	: SceneElement(parentScene),
	mInstancesBuffer(getRendererDevice()->g_Device->createBuffer(
		{ getRendererDevice()->g_GraphicsQueueFamily },
		static_cast<VkBufferUsageFlagBits>( VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT), sizeof(glm::mat4)* MaxInstancesNumber)
	),
	mCount(std::min<uint32_t>(instances.size(), MaxInstancesNumber)) {

	// Allocate memory for core buffers on the GPU exclusive memory.
	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mInstancesBuffer);

	getRendererDevice()->copyMemoryToBuffer(reinterpret_cast<const void*>(instances.data()), mInstancesBuffer, sizeof(glm::mat4)*mCount);
}

Instances::~Instances() {}

VulkanFramework::Buffer* Instances::getInstancesBuffer() noexcept {
	return mInstancesBuffer;
}

uint32_t Instances::count() const noexcept {
	return mCount;
}

const VulkanFramework::Buffer* Instances::getInstanceBuffer() const noexcept {
	return mInstancesBuffer;
}

std::vector<glm::mat4> Instances::getInstances() const noexcept {
	std::vector<glm::mat4> instances(mCount);

	getRendererDevice()->copyBufferToMemory(mInstancesBuffer, reinterpret_cast<void*>(instances.data()), sizeof(glm::mat4)*mCount);

	return std::move(instances);
}