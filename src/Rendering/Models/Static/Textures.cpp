#include "Rendering/Models/Static/Textures.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Rendering;
using namespace NeroReflex::PBRenderer::Rendering::Models;
using namespace NeroReflex::PBRenderer::Rendering::Models::Static;

Textures::Textures(
	Rendering::Scene& parentScene
) noexcept
	: SceneElement(parentScene),
	mEmptyTextureImage(
		getRendererDevice()->g_Device->createImage(
			{ getRendererDevice()->g_GraphicsQueueFamily },
			static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
			VulkanFramework::Image::ImageType::Image2D,
			1,
			1,
			1,
			NativeTextureFormat
		)
	),
	mDefaultTextureSampler(
		getRendererDevice()->g_Device->createSampler(
			defaultSamplerMagFilter,
			defaultSamplerMinFilter,
			defaultSamplerMipMapMode,
			defaultSamplerMaxAnisotropic
		)
	)
{
	std::array<float, 4> emptyData = {0.0f, 0.0f, 0.0f, 0.0f};

	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(mEmptyTextureImage);

	getRendererDevice()->copyMemoryToImage(
		reinterpret_cast<const void*>(emptyData.data()),
		mEmptyTextureImage,
		VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	);

	mEmptyTextureImageView = mEmptyTextureImage->createImageView(
		VulkanFramework::ImageView::ViewType::Image2D,
		NativeTextureFormat
	);
}

Textures::~Textures() {
	for (size_t i = 0; i < mTexturesImages.size(); ++i) {
		getRendererDevice()->g_Device->destroy(mTexturesImages[i]);
	}
}

uint32_t Textures::count() const noexcept {
	return static_cast<uint32_t>(mTexturesImages.size());
}

const std::vector<VulkanFramework::Image*>& Textures::getTexturesImage() const noexcept {
	return mTexturesImages;
}

const std::vector<VulkanFramework::ImageView*>& Textures::getTexturesImageViews() const noexcept {
	return mTexturesImageViews;
}

void Textures::addTextures(const Core::Texture& texture) noexcept {
	auto image = getRendererDevice()->g_Device->createImage(
		{ getRendererDevice()->g_GraphicsQueueFamily },
		static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
		VulkanFramework::Image::ImageType::Image2D,
		texture.getMipLevel(0).getWidth(),
		texture.getMipLevel(0).getHeight(),
		1,
		NativeTextureFormat,
		texture.countLevels()
	);

	getRendererDevice()->getMemoryPoolDeviceOnly()->malloc(image);

	auto imageView = image->createImageView(
		VulkanFramework::ImageView::ViewType::Image2D,
		NativeTextureFormat,
		VK_IMAGE_ASPECT_COLOR_BIT,
		VulkanFramework::ImageView::ViewColorMapping::rgba_rgba,
		0,
		texture.countLevels()
	);

	// Upload data to the image
	getRendererDevice()->copyTextureToImage(texture, image, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	mTexturesImages.push_back(image);
	mTexturesImageViews.push_back(imageView);
	mTexturesSamplers.push_back(mDefaultTextureSampler);
}

std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*>> Textures::getTextures(uint32_t minCount) const noexcept {
	const auto count = std::max<size_t>(mTexturesImages.size(), minCount);

	std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*>> textures(count);

	for (size_t i = 0; i < count; ++i) {
		textures[i] = std::tuple<VkImageLayout, VulkanFramework::ImageView*>(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, (i < mTexturesImageViews.size()) ? mTexturesImageViews[i] : mEmptyTextureImageView);
	}

	return textures;
}

std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> Textures::getCombinedSamplerTextures(uint32_t minCount) const noexcept {
	const auto count = std::max<size_t>(mTexturesImages.size(), minCount);

	std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> textures(count);

	for (size_t i = 0; i < count; ++i) {
		textures[i] = std::tuple<VkImageLayout, VulkanFramework::ImageView*, const VulkanFramework::Sampler*>(
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			(i < mTexturesImageViews.size()) ? mTexturesImageViews[i] : mEmptyTextureImageView,
			(i < mTexturesImageViews.size()) ? mTexturesSamplers[i] : mDefaultTextureSampler
		);
	}

	return textures;
}
