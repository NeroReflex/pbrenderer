#include "Loaders/Mesh/OBJ.h"

#include "tiny_obj_loader.h"

#define STB_IMAGE_IMPLEMENTATION

// TODO: it is possible to replace malloc, free, realloc and remove assertions
#include "stb_image.h"

#include <filesystem>

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Loaders;
using namespace NeroReflex::PBRenderer::Loaders::Mesh;

std::unique_ptr<MeshLoader::LoadResult> OBJ::load(const std::string& filename, const std::string& materialBaseDir) noexcept {
	// Reset materials
	mTexture.clear();
	mTexture.emplace("", nullptr);
	mMaterials.clear();
	mMaterials.emplace("",
		std::make_shared<MeshLoader::LoadedMtl>(
			"",
			glm::vec3(1.0f, 0.0f, 0.0f),
			mTexture[""]
		)
	);

	std::vector<tinyobj::material_t> materials;
	
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::string warn;
	std::string err;

	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename.c_str(), materialBaseDir.c_str(), true, true);
	if (!warn.empty()) {
		std::cout << "WARN: " << warn << std::endl;
	}
	if (!err.empty()) {
		std::cerr << "ERR: " << err << std::endl;
	}

	auto result = std::make_unique<MeshLoader::LoadResult>();

	std::unordered_map<std::string, MeshLoader::LoadedMesh*> meshMap;

	for (const auto& shape : shapes) {
		uint32_t vertex_index = 0;

		for (const auto& index : shape.mesh.indices) {
			const auto materialID = vertex_index / 3;
			const auto materialIndex = shape.mesh.material_ids[materialID]; 

			std::string meshCompositeName = shape.name + "/" + std::string(((materialIndex < 0) ? std::string("default_mtl") : materials[materialIndex].name ));

			const auto position = glm::vec3(
				attrib.vertices[(index.vertex_index * 3) + 0],
				attrib.vertices[(index.vertex_index * 3) + 1],
				attrib.vertices[(index.vertex_index * 3) + 2]
			);

			const auto normal = /*((index.normal_index >= 0) && (attrib.normals.size() > (index.normal_index * 3 + 2))) ?*/ glm::vec3(
				attrib.normals[(index.normal_index * 3) + 0],
				attrib.normals[(index.normal_index * 3) + 1],
				attrib.normals[(index.normal_index * 3) + 2]
			) /*: glm::vec3(0, 0, 0)*/;

			const auto texture_uv = /*((index.texcoord_index >= 0) && (attrib.texcoords.size() > (index.texcoord_index * 2 + 1))) ?*/ glm::vec2(
				attrib.texcoords[(index.texcoord_index * 2) + 0],
				attrib.texcoords[(index.texcoord_index * 2) + 1]
			) /*: glm::vec2(0.0f, 0.0f)*/;

			MeshLoader::LoadedMesh* mesh = meshMap[meshCompositeName];
			
			// Load a material: either the specified one or the default one
			auto material = (materialIndex >= 0)
				? loadMaterial(
					materialBaseDir,
					materials[materialIndex].name,
					glm::vec3(materials[materialIndex].diffuse[0], materials[materialIndex].diffuse[1], materials[materialIndex].diffuse[2]),
					materials[materialIndex].diffuse_texname
				)
				: std::make_shared<MeshLoader::LoadedMtl>();
			
			// If the mesh has not been registered (yet) than do it now
			if (!mesh) {
				meshMap[meshCompositeName] = mesh = new MeshLoader::LoadedMesh(material);
				result->push_model(meshCompositeName, std::unique_ptr<MeshLoader::LoadedMesh>(mesh));
			}

			mesh->push_vertex(
				Core::Vertex(
					glm::vec4(position, 1.0f),
					glm::vec4(normal, 0.0f),
					texture_uv
				)
			);
			
			vertex_index++;
		}
	}

	return result;
}

std::shared_ptr<MeshLoader::LoadedMtl> OBJ::loadMaterial(
	const std::string& path,
	const std::string& name,
	const glm::vec3& diffuseColor,
	const std::string& diffuseTextureName
) noexcept {
	if (mMaterials[name])
		return mMaterials[name];

	auto loadedMaterial = std::make_shared<MeshLoader::LoadedMtl>(name, diffuseColor, loadTexture(path, diffuseTextureName));

	return mMaterials[name] = std::move(loadedMaterial);
}

std::shared_ptr<Core::Texture> OBJ::loadTexture(const std::string& path, const std::string& name) noexcept {
	if (mTexture.find(name) != mTexture.cend())
		return mTexture[name];

	std::shared_ptr<Core::Texture> loadedTexture = mTexture[""];
	if (std::filesystem::exists(path + name)) {
		std::string filename = path + name;

		int x, y, n;
		
#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
		float* data = stbi_loadf(filename.c_str(), &x, &y, &n, 4);
#else
		unsigned char* data = stbi_load(filename.c_str(), &x, &y, &n, 4);
#endif
		std::vector<Core::Texture::TextureDataType> rgba(x * y);
		std::memcpy(reinterpret_cast<void*>(rgba.data()), reinterpret_cast<const void*>(data), sizeof(Core::Texture::TextureDataType)* rgba.size());

		stbi_image_free(data);

		loadedTexture = std::make_shared<Core::Texture>(x, y, std::move(rgba));
	}

	return mTexture[name] = std::move(loadedTexture);
}