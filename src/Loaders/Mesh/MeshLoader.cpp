#include "Loaders/Mesh/MeshLoader.h"

using namespace NeroReflex;
using namespace NeroReflex::PBRenderer;
using namespace NeroReflex::PBRenderer::Loaders;
using namespace NeroReflex::PBRenderer::Loaders::Mesh;

MeshLoader::LoadResult::LoadResult() noexcept {}

MeshLoader::LoadedMtl::LoadedMtl(
	const std::string& name,
	const glm::vec3& diffuseColor,
	const std::shared_ptr<Core::Texture>& diffuseTexture
) noexcept
	: mName(name),
	mDiffuseColor(diffuseColor),
	mDiffuseTexture(diffuseTexture) {}

const std::string& MeshLoader::LoadedMtl::getName() const noexcept {
	return mName;
}

const glm::vec3& MeshLoader::LoadedMtl::getDiffuseColor() const noexcept {
	return mDiffuseColor;
}

Core::Material MeshLoader::LoadedMtl::getNativeMaterial() const noexcept {
	return Core::Material(mDiffuseColor);
}

const Core::Texture* MeshLoader::LoadedMtl::getDiffuseTexture() const noexcept {
	return mDiffuseTexture.get();
}

MeshLoader::LoadedMesh::LoadedMesh(const std::shared_ptr<LoadedMtl>& material) noexcept
	: mMaxAABB(glm::vec3(0, 0, 0)),
	mMinAABB(glm::vec3(0, 0, 0)),
	mMaterial(material) {}

const std::vector<uint32_t>& MeshLoader::LoadedMesh::getIndeces() const noexcept {
	return mIndices;
}

const std::vector<Core::Vertex>& MeshLoader::LoadedMesh::getVertexData() const noexcept {
	return mVertexData;
}

MeshLoader::MeshLoader(std::unique_ptr<FormatReader> formatReader) noexcept
	: mFormatReader(std::move(formatReader)) {}

std::unique_ptr<MeshLoader::LoadResult> MeshLoader::load(const std::string& filename, const std::string& materialBaseDir) const noexcept {
	return mFormatReader->load(filename, materialBaseDir);
}

void MeshLoader::LoadedMesh::push_vertex(const Core::Vertex& vertex) noexcept {
	const auto vPosition = vertex.getPosition();

	// Update the AABB
	if (mVertexData.empty())
		mMinAABB = mMaxAABB = vPosition;

	mMinAABB.x = std::min(mMinAABB.x, vPosition.x);
	mMinAABB.y = std::min(mMinAABB.y, vPosition.y);
	mMinAABB.z = std::min(mMinAABB.x, vPosition.z);
	mMaxAABB.x = std::max(mMaxAABB.x, vPosition.x);
	mMaxAABB.y = std::max(mMaxAABB.y, vPosition.y);
	mMaxAABB.z = std::max(mMaxAABB.x, vPosition.z);

	const auto searchResult = std::find(mVertexData.cbegin(), mVertexData.cend(), vertex);
	
	const uint32_t index = std::distance(mVertexData.cbegin(), searchResult);

	mIndices.emplace_back(index);

	if (searchResult == mVertexData.cend()) {
		mVertexData.emplace_back(vertex);
	}
}

Core::AABB MeshLoader::LoadedMesh::getAABB() const noexcept {
	return Core::AABB(mMinAABB, mMaxAABB);
}

const MeshLoader::LoadedMtl* MeshLoader::LoadedMesh::getMaterial() const noexcept {
	return &(*mMaterial);
}

void MeshLoader::LoadResult::push_model(const std::string& name, std::unique_ptr<LoadedMesh>&& mesh) noexcept {
	mMeshes.emplace(name, std::move(mesh));
}

const std::unordered_map<std::string, std::unique_ptr<MeshLoader::LoadedMesh>>& MeshLoader::LoadResult::getMeshes() const noexcept {
	return mMeshes;
}

void MeshLoader::LoadResult::foreachLoadedMesh(std::function<void(const std::string&, const LoadedMesh&)> fn) const noexcept {
	for (const auto& mesh : mMeshes) {
		fn(mesh.first, *(mesh.second));
	}
}
