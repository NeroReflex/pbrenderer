# PBRenderer

A physically based rendering engine that uses vulkan.

## Compilation

To compile for the current platform:
```sh
mkdir build
cd build
cmake ..
cmake --build .
```

## Compile for windows
As I refuse to use windows I compile from arch:


First install required tools with
```sh
paru -S mingw-w64-cmake mingw-w64-gcc mingw-w64-binutils
```

And then use the mingw cmake to generate cmake cache to cross-compile and make:
```sh
mkdir build-mingw
cd build-mingw
x86_64-w64-mingw32-cmake -G "Unix Makefiles" ../
make all
```