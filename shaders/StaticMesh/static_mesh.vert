#version 450

#define Z_PREPASS_PIPELINE 1
#include "../binding.glsl"

layout (location = 0) in vec4 vPosition_modelspace;
layout (location = 1) in vec4 vNormal_modelspace;
layout (location = 2) in vec2 vTextureUV;
layout (location = 3) in uint vMaterialIndex;

layout (location = 4) in vec4 ModelMatrix_first_row;
layout (location = 5) in vec4 ModelMatrix_second_row;
layout (location = 6) in vec4 ModelMatrix_third_row;
layout (location = 7) in vec4 ModelMatrix_fourth_row;

layout (location = 0) out vec4 out_vPosition_worldspace_minus_eye_position;
layout (location = 1) out vec4 out_vNormal_worldspace;
layout (location = 2) out vec2 out_vTextureUV;
layout (location = 3) out flat uint out_vMaterialIndex;

layout(location = 4) out flat vec4 eyePosition_worldspace;

layout( push_constant ) uniform camera_uniform {
	mat4 viewMatrix;
	mat4 projectionMatrix;
} camera;


void main() {
	const mat4 ModelMatrix = mat4(ModelMatrix_first_row, ModelMatrix_second_row, ModelMatrix_third_row, ModelMatrix_fourth_row);

	// This is the MVP matrix
	const mat4 MVP = camera.projectionMatrix * camera.viewMatrix * ModelMatrix;

	// Get the eye position
	const vec4 eye_position = vec4(camera.viewMatrix[3][0], camera.viewMatrix[3][1], camera.viewMatrix[3][2], 1.0);
	eyePosition_worldspace = eye_position;

	out_vMaterialIndex = vMaterialIndex;

	vec4 vPosition_worldspace = ModelMatrix * vPosition_modelspace;
	vPosition_worldspace /= vPosition_worldspace.w;

	out_vTextureUV = vec2(vTextureUV.x, 1-vTextureUV.y);
	out_vPosition_worldspace_minus_eye_position = vec4((vPosition_worldspace - eyePosition_worldspace).xyz, 1.0);
	out_vNormal_worldspace = vec4((ModelMatrix * vNormal_modelspace).xyz, 0.0);

	gl_Position = MVP * vPosition_modelspace;
}
