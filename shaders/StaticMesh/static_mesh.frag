#version 450

#define Z_PREPASS_PIPELINE
#include "../binding.glsl"

#include "../config.glsl"
#include "../materials.glsl"

layout (location = 0) in vec4 in_vPosition_worldspace_minus_eye_position;
layout (location = 1) in vec4 in_vNormal_worldspace;
layout (location = 2) in vec2 in_vTextureUV;
layout (location = 3) in flat uint in_vMaterialIndex;

layout(location = 4) in flat vec4 in_eyePosition_worldspace;

// =================== FRAGMENT OUTPUT =========================
layout (location = 0) out vec4 out_vPosition;
layout (location = 1) out vec4 out_vNormal;
layout (location = 2) out vec4 out_vDiffuseAlbedo;
// =============================================================

void main() {
    // Calculate position of the current fragment
    const vec4 vPosition_worldspace = vec4((in_vPosition_worldspace_minus_eye_position + in_eyePosition_worldspace).xyz, 1.0);

    vec3 dFdxPos = dFdx( in_vPosition_worldspace_minus_eye_position.xyz );
	vec3 dFdyPos = dFdy( in_vPosition_worldspace_minus_eye_position.xyz );
	const vec3 facenormal = cross(dFdxPos, dFdyPos);

    // The normal can either be calculated or provided from the mesh. Just pick the provided one if it is valid.
    const vec3 bestNormal = normalize(length(in_vNormal_worldspace.xyz) < 0.000001f ? facenormal : in_vNormal_worldspace.xyz);

    out_vPosition = vPosition_worldspace;
    out_vNormal = vec4(bestNormal.xyz, 0.0);
    out_vDiffuseAlbedo = vec4(getDiffuseMaterialAlbedo(in_vMaterialIndex, in_vTextureUV)) / 255.0;
}