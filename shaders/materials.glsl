#include "./config.glsl"

struct Material {
	vec4 diffuseColor;

	uint diffuseTextureIndex;

	uint materialFlags;
};

layout (binding = MATERIAL_BUFFER_BINDING) readonly buffer materials_buffer {
	Material materials[];
} materials;

#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
layout(binding = MATERIAL_TEXTURES_BINDING) uniform sampler2D textures[MAX_TEXTURE_IN_ARRAY_COUNT];
#else
layout(binding = MATERIAL_TEXTURES_BINDING) uniform usampler2D textures[MAX_TEXTURE_IN_ARRAY_COUNT];
#endif

uvec4 textureLookup(in const uint textureID, in const vec2 uv) {
#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
		const vec4 result = texture(textures[textureID], uv) * 255.0;
#else
		uvec4 result = texture(textures[textureID], uv);
		//const vec4 result = vec4(float(textureLookup.r), float(textureLookup.g), float(textureLookup.b), float(textureLookup.a)) / 255.0;
#endif

/*
	const float exposure = 1.0;
	const float gamma = 2.2;
    vec3 hdrColor = result.rgb;
  
    // Exposure tone mapping
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
    // Gamma correction 
    mapped = pow(mapped, vec3(1.0 / gamma));

	return vec4(mapped, 1.0);
*/
	return result;
}

uvec4 getDiffuseMaterialAlbedo(in uint materialIndex, in vec2 uv) {
	uvec3 diffuseAlbedoFromMaterial = uvec3(materials.materials[materialIndex].diffuseColor.rgb * 255.0);

	if ((materials.materials[materialIndex].materialFlags & MATERIAL_FLAGS_HAS_TEXTURE) != 0x00000000) {

		const uvec4 diffuseAlbedoFromTexture = textureLookup(materials.materials[materialIndex].diffuseTextureIndex, uv);

#if MIX_MATERIAL_AND_TEXTURE_WITH_ALPHA
		return uvec4((diffuseAlbedoFromTexture.a*diffuseAlbedoFromTexture.rgb) + ((255-diffuseAlbedoFromTexture.a)*diffuseAlbedoFromMaterial), 255);
#else
		return diffuseAlbedoFromTexture.rgba;
#endif
	}
	
	return uvec4(diffuseAlbedoFromMaterial, 255);
}