#version 450

#define DEFERRED_SHADING_PIPELINE 1
#include "../binding.glsl"

layout (location = 0) out vec4 presentedColor;

#include "../directional_lighting.glsl"

layout( push_constant ) uniform deferred_uniform {
	uint directionalLightsCount;
} deferred;

layout (std140, binding = HDR_UNIFORM_BINDING) uniform HDR_uniform {
	float gamma;
	float exposure;
} hdr;

layout (input_attachment_index = 0, set = 0, binding = GBUFFER_FB2_BINDING) uniform subpassInput gbuffer_vPosition;
layout (input_attachment_index = 1, set = 0, binding = GBUFFER_FB3_BINDING) uniform subpassInput gbuffer_vNormal;
layout (input_attachment_index = 2, set = 0, binding = GBUFFER_FB4_BINDING) uniform subpassInput gbuffer_vDiffuseAlbedo;

void main() {
    const vec3 in_vPosition_worldspace = subpassLoad(gbuffer_vPosition).xyz;
    const vec3 in_vNormal_worldspace = subpassLoad(gbuffer_vNormal).xyz ;
    const vec4 in_vDiffuseAlbedo = subpassLoad(gbuffer_vDiffuseAlbedo);

    vec4 hdrColor = vec4(0.0, 0.0, 0.0, in_vDiffuseAlbedo.a);

#if INCLUDE_DIRECT_ILLUMINATION
    // sum all directional light contribution
    for (uint directionalLightIndex = 0; directionalLightIndex < deferred.directionalLightsCount; ++directionalLightIndex) {
        hdrColor.rgb += directionalLightShadowmappedContribution(in_vPosition_worldspace, in_vNormal_worldspace, in_vDiffuseAlbedo.rgb, directionalLightIndex);
    }
#endif

#if HDR_MAPPING
    // Exposure tone mapping
    vec3 mapped = vec3(1.0) - exp(-(hdrColor.rgb) * hdr.exposure);
    // Gamma correction 
    mapped = pow(mapped, vec3(1.0 / hdr.gamma));
#else
    const vec3 mapped = vec3(hdrColor.xyz);
#endif

	presentedColor = vec4(mapped, in_vDiffuseAlbedo.a);
}
