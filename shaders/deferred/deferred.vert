#version 450

#define DEFERRED_SHADING_PIPELINE 1
#include "../binding.glsl"

const vec2 vQuadPosition[6] = {
	vec2(-1, -1),
	vec2(+1, -1),
	vec2(-1, +1),
	vec2(-1, +1),
	vec2(+1, +1),
	vec2(+1, -1),
};

void main() {
	gl_Position = vec4(vQuadPosition[gl_VertexIndex], 0.0, 1.0);
}