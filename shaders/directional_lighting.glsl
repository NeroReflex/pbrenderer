#ifndef LIGHTING_GLSL
#define LIGHTING_GLSL

#include "./config.glsl"

/**
 * albedoAndIntensity.xyz has every component between [-1...1] and there is at least one component that is either -1 or 1.
 * albedoAndIntensity.a is the intensity.
 */
struct DirectionalLightType {
	mat4 worldspace_to_interest_space;

	mat4 interest_space_to_normalized_space;

	mat4 view;

	mat4 projection;

	vec4 direction;

	vec4 albedoAndIntensity;
};


layout (std140, binding = LIGHTING_DIRECTIONAL_LIGHT_BINDING) readonly uniform direcitonal_lights_uniform {
	DirectionalLightType light[MAX_DIRECTIONAL_LIGHTS];
} directionalLights;

vec3 translateWorldspaceToNormalizedInterestspace(in const vec3 worldPosition, in const uint directionalLightIndex) {
	vec4 fragPos = (directionalLights.light[directionalLightIndex].interest_space_to_normalized_space * directionalLights.light[directionalLightIndex].worldspace_to_interest_space) * vec4(worldPosition, 1.0);
	fragPos = fragPos / fragPos.w;

	return fragPos.xyz;
}

vec3 translateVectorWorldspaceToNormalizedInterestspace(in const vec3 worldPosition, in const uint directionalLightIndex) {
	vec4 fragPos = (directionalLights.light[directionalLightIndex].interest_space_to_normalized_space * directionalLights.light[directionalLightIndex].worldspace_to_interest_space) * vec4(worldPosition, 0.0);

	return fragPos.xyz;
}

vec4 voxelspaceToDirectionalLightspace(in const vec3 vPosition_voxelspace, in const uint directionalLightIndex) {
	const mat4 LightVPMatrix = directionalLights.light[directionalLightIndex].projection * directionalLights.light[directionalLightIndex].view;

	return LightVPMatrix * vec4(vPosition_voxelspace, 1.0);
}

#if defined(LIGHTING_DIRECTIONAL_LIGHT_USE_SHADOWMAPS)
	layout(binding = LIGHTING_DIRECTIONAL_LIGHT_SHADOWMAPS_BINDING) uniform sampler2D directionalShadowmaps[MAX_DIRECTIONAL_LIGHTS];

	float sampleDirectionalLightShadowmap(in const vec3 vPosition_voxelspace, const float bias, in const uint directionalLightIndex) {
		const mat4 LightVPMatrix = directionalLights.light[directionalLightIndex].projection * directionalLights.light[directionalLightIndex].view;

		vec4 vPosition_lightspace = voxelspaceToDirectionalLightspace(vPosition_voxelspace, directionalLightIndex);
		vPosition_lightspace /= vPosition_lightspace.w;

		const vec2 shadowmapUV = (vPosition_lightspace.xy + 1.0) / 2.0;

		const vec2 texelSize = 1.0 / textureSize(directionalShadowmaps[directionalLightIndex], 0);
		float accumulatedLightContribution = 0.0;
		for (int i = -1; i <= 1; ++i) {
			for (int j = -1; j <= 1; ++j) {
				const vec2 shadowmapSampleUVOffset = texelSize * vec2(i, j);
				
				const float pcfDepth = texture(directionalShadowmaps[directionalLightIndex], shadowmapUV + shadowmapSampleUVOffset).r;

				accumulatedLightContribution += (voxelspaceToDirectionalLightspace(vPosition_voxelspace, directionalLightIndex).z - bias > pcfDepth) ? 0.0 : 1.0;
			}
		}

		return accumulatedLightContribution / 9.0;
	}

	float intensityShadowmappedContribution(in const vec3 vPosition_voxelspace, in const vec3 vNormal_voxelspace, in const uint directionalLightIndex) {
		const float intensity = directionalLights.light[directionalLightIndex].albedoAndIntensity.a;

		const float scalingFactor = max(0.0, dot(-directionalLights.light[directionalLightIndex].direction.xyz, vNormal_voxelspace));

		return intensity * scalingFactor;
	}

	vec3 directionalLightContribution(in const vec3 vPosition_voxelspace, in const vec3 vNormal_voxelspace, in const vec3 vAlbedo, in const uint directionalLightIndex) {
		const vec3 colorContributionWithoutIntensity = directionalLights.light[directionalLightIndex].albedoAndIntensity.rgb * vAlbedo.rgb;
		
		const float shadowmappedIntensity = intensityShadowmappedContribution(vPosition_voxelspace, vNormal_voxelspace, directionalLightIndex);

		const vec3 colorContribution = colorContributionWithoutIntensity * shadowmappedIntensity;

		return colorContribution.rgb;
	}


	vec3 directionalLightShadowmappedContribution(in const vec3 vPosition_worldspace, in const vec3 vNormal_worldspace, in const vec3 vAlbedo, in const uint directionalLightIndex) {
		const vec3 vPosition_normalized_interestspace = translateWorldspaceToNormalizedInterestspace(vPosition_worldspace.xyz, directionalLightIndex);

		const vec3 vNormal_normalized_interestspace = translateVectorWorldspaceToNormalizedInterestspace(vNormal_worldspace.xyz, directionalLightIndex);

		const vec3 lightContributionWithoutShadow = directionalLightContribution(vPosition_normalized_interestspace, vNormal_normalized_interestspace, vAlbedo, directionalLightIndex);

		const float bias = max(SHADOWMAP_MAX_BIAS * (1.0 - dot(vNormal_normalized_interestspace, -directionalLights.light[directionalLightIndex].direction.xyz)), SHADOWMAP_MIN_BIAS);  
//		const float bias = clamp(0.005 * tan(acos(1.0 - dot(vNormal_normalized_interestspace, -directionalLights.light[directionalLightIndex].direction.xyz))), 0.0, 0.01);  

		// If the area is not covered by the shadowmap shadows won't be casted, but the area will be illuminated to avoid gaps and light discontinuity
		if (
			((vPosition_normalized_interestspace.x > 1.0) || (vPosition_normalized_interestspace.x < -1.0)) ||
			((vPosition_normalized_interestspace.y > 1.0) || (vPosition_normalized_interestspace.y < -1.0)) ||
			((vPosition_normalized_interestspace.z > 1.0) || (vPosition_normalized_interestspace.z < -1.0))
		) {
			return lightContributionWithoutShadow;
		}

		const float shadowmapLightingFactor = sampleDirectionalLightShadowmap(vPosition_normalized_interestspace, bias, directionalLightIndex);

        return lightContributionWithoutShadow * shadowmapLightingFactor ;
	}
#endif

#endif