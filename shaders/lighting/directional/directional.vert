#version 450

#define DIRECTIONAL_LIGHTING_PIPELINE
#include "../../binding.glsl"

#include "../../config.glsl"
#include "../../directional_lighting.glsl"

layout (location = 0) in vec4 vPosition_modelspace;
layout (location = 1) in vec4 vNormal_modelspace;
layout (location = 2) in vec2 vTextureUV;
layout (location = 3) in uint vMaterialIndex;

layout (location = 4) in vec4 ModelMatrix_first_row;
layout (location = 5) in vec4 ModelMatrix_second_row;
layout (location = 6) in vec4 ModelMatrix_third_row;
layout (location = 7) in vec4 ModelMatrix_fourth_row;

layout (location = 0) out vec4 out_vPosition_worldspace_minus_eye_position;
layout (location = 1) out vec4 out_vNormal_worldspace;
layout (location = 2) out vec2 out_vTextureUV;
layout (location = 3) out flat uint out_vMaterialIndex;

layout( push_constant ) uniform light_uniform {
	uint index;
} currentDirectionalLight;

void main() {
	const mat4 ModelMatrix = mat4(ModelMatrix_first_row, ModelMatrix_second_row, ModelMatrix_third_row, ModelMatrix_fourth_row);

	vec4 vPosition_worldspace = ModelMatrix * vPosition_modelspace;
	vPosition_worldspace /= vPosition_worldspace.w;

	const vec3 vPosition_normalized_interestspace = translateWorldspaceToNormalizedInterestspace(vPosition_worldspace.xyz, currentDirectionalLight.index);
	
	gl_Position = voxelspaceToDirectionalLightspace(vPosition_normalized_interestspace, currentDirectionalLight.index);
}
