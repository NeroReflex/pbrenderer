#pragma once

#include "Rendering/RenderingSurface.h"

#include "Core/HDR.h"
#include "Core/Vertex.h"
#include "Core/Lighting/PointLight.h"

#include "Loaders/Mesh/MeshLoader.h"

#include "Rendering/Device/RendererDevice.h"

#include "Rendering/SceneElement.h"

#include "Rendering/Models/Static/Textures.h"
#include "Rendering/Models/Static/Materials.h"
#include "Rendering/Models/Static/ConcreteCollection.h"

#include "Rendering/Pipeline/FinalRenderingPipeline.h"
#include "Rendering/Pipeline/DirectionalLightingPipeline.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {

			class Scene {

				friend class RenderingSurface;

				friend class SceneElement;

			public:
				typedef std::unordered_map<std::string, std::unique_ptr<Models::Static::ConcreteCollection>> StaticMeshCollection;

				Scene(
					const Rendering::RenderingSurface& window
				) noexcept;

				Scene(const Scene&) = delete;

				Scene(Scene&&) = delete;

				Scene& operator=(const Scene&) = delete;

				virtual ~Scene() = default;

				void loadStaticMesh(const std::string& name, const Loaders::Mesh::MeshLoader::LoadResult& meshAsLoadResult, std::vector<glm::mat4> instances = {}) noexcept;

				void setHDR(const Core::HDR& hdr) const noexcept;

				void setProjectionMatrix(const glm::mat4& projection) noexcept;

				void setViewMatrix(const glm::mat4& view) noexcept;

				void setViewProjectionMatrices(const glm::mat4& view, const glm::mat4& projection) noexcept;

				const VulkanFramework::Utils::SurfaceDimensions& getSurfaceDimensions() const noexcept;

				Core::AABB getAABB() const noexcept;

				void addPointLights(const std::vector<Core::Lighting::PointLight>& lights) noexcept;

				void addDirectionalLights(const std::vector<Core::Lighting::DirectionalLight>& directionalLights) noexcept;

			protected:
				virtual void onResize(uint32_t newWidth, uint32_t newHeight) noexcept;

				void onRender() noexcept;

				const Rendering::RenderingSurface& mRenderingSurface;

				std::unique_ptr<Device::RendererDevice> mRendererDevice;

				std::unique_ptr<Pipeline::DirectionalLightingPipeline> mDirectionalLightingPipeline;

				std::unique_ptr<Pipeline::FinalRenderingPipeline> mFinalRenderingPipeline;

				std::unique_ptr<Models::Static::Textures> mStaticMeshesTextures;

				std::unique_ptr<Models::Static::Materials> mStaticMeshesMaterials;

				StaticMeshCollection mStaticMeshes;

				glm::mat4 mViewMatrix;

				glm::mat4 mProjectionMatrix;

			private:
				uint32_t mTwoExpOfVoxelCount;

				std::unordered_map<uintptr_t, uint32_t> mLoadedMaterialsMap;

				std::vector<uintptr_t> mLoadedMaterials;

				std::vector<uintptr_t> mLoadedTextures;

				static constexpr VkFormat DepthStencilImageFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
			};

		}
	}
}