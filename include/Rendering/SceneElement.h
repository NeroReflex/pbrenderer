#pragma once

#include "PBRenderer.h"

#include "Rendering/Device/RendererDevice.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {

			class Scene;

			class SceneElement {

			public:
				SceneElement(Rendering::Scene& parentScene) noexcept;

				SceneElement() = delete;

				SceneElement(const SceneElement&) = delete;

				SceneElement(SceneElement&&) = delete;

				SceneElement& operator=(const SceneElement&) = delete;

				virtual ~SceneElement() = default;

				Scene& getScene() noexcept;

				const Scene& getScene() const noexcept;

			protected:
				Device::RendererDevice* getRendererDevice() const noexcept;

			private:
				Rendering::Scene& mScene;
			};

		}
	}
}