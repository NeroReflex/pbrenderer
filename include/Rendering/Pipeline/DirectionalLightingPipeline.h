#pragma once

#include "Rendering/SceneElement.h"

#include "Core/Vertex.h"

#include "Rendering/Models/Static/ConcreteCollection.h"

#include "Core/Lighting/DirectionalLight.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Pipeline {
				/**
				 * Contains the pipeline used to generate shadow maps.
				 */
				class DirectionalLightingPipeline :
					virtual public Rendering::SceneElement {

				public:
					static constexpr VkImageLayout ShadowmapResultImageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

					static constexpr uint32_t MaxDirectionalLights = MAX_DIRECTIONAL_LIGHTS;

					/**
					 * Create a directional light pipeline for shadow maps generation.
					 */
					DirectionalLightingPipeline(
						Rendering::Scene& parentScene,
						glm::uint expOfTwo /*= 11*/
					) noexcept;

					DirectionalLightingPipeline(const DirectionalLightingPipeline&) = delete;

					DirectionalLightingPipeline(DirectionalLightingPipeline&&) = delete;

					DirectionalLightingPipeline& operator=(const DirectionalLightingPipeline&) = delete;

					~DirectionalLightingPipeline() override;

					void generateShadowMaps(
						const std::unordered_map<std::string, std::unique_ptr<Models::Static::ConcreteCollection>>& staticModels
					) noexcept;

					uint32_t getLightsCount() const noexcept;

					void addDirectionalLights(const std::vector<Core::Lighting::DirectionalLight>& directionalLights) noexcept;

					const VulkanFramework::Semaphore* getSemaphore() const noexcept;

					const VulkanFramework::Buffer* getDirectionalLightingUniformBuffer() const noexcept;

					std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> getCombinedSamplerShadowmaps() const noexcept;

					const VulkanFramework::Fence* getFence() const noexcept;
				private:
					
					// Used to structure data on buffer in vulkan
					struct std140DirectionalLightData {
						
					private:
						/*alignas (sizeof(glm::mat4))*/ glm::mat4 worldspace_to_interest_space;

						/*alignas (sizeof(glm::mat4))*/ glm::mat4 interest_space_to_normalized_space;

						/*alignas (sizeof(glm::mat4))*/ glm::mat4 view;

						/*alignas (sizeof(glm::mat4))*/ glm::mat4 projection;

						/*alignas (sizeof(glm::vec4))*/ glm::vec4 direction;

						/*alignas (sizeof(glm::vec4))*/ glm::vec4 albedoAndIntensity;

					public:
						std140DirectionalLightData() = default;

						std140DirectionalLightData(const Core::Lighting::DirectionalLight& directionalLight, glm::uint resolutionExpOfTwo) noexcept
							: view(glm::lookAt(directionalLight.getDirection() * -1.0f, glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0))),
							//projection(glm::ortho(-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f)),
							projection(glm::ortho(-1.0f, 1.0f, 1.0f, -1.0f, -SQRT_3, SQRT_3)),
							direction(glm::vec4(directionalLight.getDirection(), 0.0)),
							albedoAndIntensity(glm::vec4(directionalLight.getAlbedo(), directionalLight.getIntensity())) {

								const glm::float32 absMaxAlbedoComponent = std::max(std::max(std::abs(albedoAndIntensity.r), std::abs(albedoAndIntensity.g)), std::abs(albedoAndIntensity.b));
								
								albedoAndIntensity.r = albedoAndIntensity.r / absMaxAlbedoComponent;
								albedoAndIntensity.g = albedoAndIntensity.g / absMaxAlbedoComponent;
								albedoAndIntensity.b = albedoAndIntensity.b / absMaxAlbedoComponent;
								albedoAndIntensity.a = albedoAndIntensity.a * absMaxAlbedoComponent;

								const auto aabb = directionalLight.getInterestedVolume();
								const auto minPoint = aabb.getMinimumPoint();
								const auto maxPoint = aabb.getMaximumPoint();
								
								const auto dimensions = maxPoint - minPoint;
								const auto halfDimensions = dimensions / 2.0f;

								const auto voxelCount = glm::uint(1) << resolutionExpOfTwo;

								glm::float32 voxelDimension = std::max<glm::float32>(dimensions.x, dimensions.y);
								voxelDimension = std::max<glm::float32>(voxelDimension, dimensions.z);
								voxelDimension = voxelDimension / static_cast<float>(voxelCount);

								const auto halfVoxelCount = static_cast<float>(voxelCount) / 2.0f;

								worldspace_to_interest_space = glm::translate(
										glm::mat4(1.0f),
										-1.0f * (aabb.getMinimumPoint() + halfDimensions)
									);

								interest_space_to_normalized_space = glm::ortho<glm::float32>(
									voxelDimension * halfVoxelCount,
									voxelDimension * -halfVoxelCount,

									voxelDimension* halfVoxelCount,
									voxelDimension * -halfVoxelCount,
									
									voxelDimension * -halfVoxelCount,
									voxelDimension * halfVoxelCount
								);

							};

						/*std140DirectionalLightData& operator=(const std140DirectionalLightData& src) {
							if (this != &src) {
								view = src.view;
								projection = src.projection;
								direction = src.direction;
								albedoAndIntensity = src.albedoAndIntensity;
							}

							return *this;
						}*/

					};

					constexpr static size_t DirectionalLightsUniformBufferSize = sizeof(std140DirectionalLightData) * MaxDirectionalLights;

					uint32_t mResolutionExpOfTwo;

					std::array<std140DirectionalLightData, MaxDirectionalLights> mDirectionalLights;

					glm::uint32 mOccupiedDirectionalLights;

					const VulkanFramework::Utils::SurfaceDimensions mShadowMapResolution;

					VulkanFramework::Buffer* mDirectionalLightsUniformBuffer;

					VulkanFramework::CommandBuffer* mCommandBuffer;

					VulkanFramework::RenderPass* const mPipelineRenderPass;

					VulkanFramework::GraphicPipeline* const mPipeline;

					VulkanFramework::DescriptorPool* const mDescriptorPool;

					VulkanFramework::DescriptorSet* mDescriptorSet;

					VulkanFramework::Sampler* const mShadowMappingResultSampler;

					VulkanFramework::Fence* const mFence;

					VulkanFramework::Semaphore* mSemaphore;

					std::array<VulkanFramework::Image*, MaxDirectionalLights> mShadowMappingResult;
					std::array<VulkanFramework::ImageView*, MaxDirectionalLights> mShadowMappingResultView;
					
					std::array<VulkanFramework::Framebuffer*, MaxDirectionalLights> mFramebuffer;

					static constexpr VkFormat ShadowMapImageFormat = VK_FORMAT_D32_SFLOAT;
				};

			}
		}
	}
}