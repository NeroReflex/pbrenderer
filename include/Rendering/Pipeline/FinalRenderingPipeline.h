#pragma once

#include "Sampler.h"

#include "Core/HDR.h"
#include "Core/Vertex.h"

#include "Rendering/SceneElement.h"
#include "Rendering/Models/Static/ConcreteCollection.h"
#include "Rendering/Pipeline/DirectionalLightingPipeline.h"

#include "Rendering/Models/Static/Textures.h"
#include "Rendering/Models/Static/Materials.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Pipeline {

				/**
				 * Contains the final render pipeline used to generate the resulting image to be presented.
				 */
				class FinalRenderingPipeline :
					virtual public Rendering::SceneElement {

					struct CameraBufferSTD140 {
						alignas(sizeof(glm::mat4)) glm::mat4 projectionMatrix;

						alignas(sizeof(glm::mat4)) glm::mat4 viewMatrix;
					};

				public:

					FinalRenderingPipeline(
						Rendering::Scene& parentScene
					) noexcept;

					FinalRenderingPipeline(const FinalRenderingPipeline&) = delete;

					FinalRenderingPipeline(FinalRenderingPipeline&&) = delete;

					FinalRenderingPipeline& operator=(const FinalRenderingPipeline&) = delete;

					~FinalRenderingPipeline() override;

					void setHDR(const Core::HDR& hdr) noexcept;

					void executeRendering(
						DirectionalLightingPipeline* directionalLightingPipeline,
						glm::mat4 viewMatrix,
						glm::mat4 projectionMatrix,
						const Models::Static::Textures& textures,
						const Models::Static::Materials& materials,
						const std::unordered_map<std::string, std::unique_ptr<Models::Static::ConcreteCollection>>& staticModels
					) noexcept;

					void waitIdle() const noexcept;

				private:
					Core::HDR mHDR;

					std::array<VulkanFramework::DescriptorPool*, 2> mDescriptorPools;

					std::vector<std::array<VulkanFramework::Image*, 3>> mRenderingGBuffer;

					std::vector<std::array<VulkanFramework::ImageView*, 3>> mRenderingGBufferImageViews;

					std::vector<VulkanFramework::Image*> mRenderingDepthStencilImages;

					std::vector<VulkanFramework::Framebuffer*> mRenderingFramebuffers;

					std::vector<VulkanFramework::Semaphore*> mImageAvailableSemaphores;

					std::vector<VulkanFramework::Semaphore*> mImageRenderedSemaphores;

					std::vector<VulkanFramework::Fence*> mRenderingFences;

					std::vector<VulkanFramework::CommandBuffer*> mRenderingCommandBuffers;

					VulkanFramework::RenderPass* const mRenderingPipelineRenderPass;
					
					VulkanFramework::GraphicPipeline* mRenderingPipeline;

					VulkanFramework::GraphicPipeline* mDeferredPipeline;

					std::vector<std::array<VulkanFramework::DescriptorSet*, 2>> mRenderingDescriptorSets;

					std::vector<VulkanFramework::Buffer*> mHDRBuffer;

					uint32_t mCurrentFrame = 0;

					static constexpr VkFormat DepthStencilImageFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;

					//static constexpr VkFormat ColorAttachmentImageFormat = VK_FORMAT_R16G16B16A16_UNORM;
					static constexpr VkFormat ColorAttachmentImageFormat = VK_FORMAT_R32G32B32A32_SFLOAT;

					struct pushConstantData {
						pushConstantData(const glm::uint32& directionalLightsCount = 0) noexcept : directionalLightsCount(directionalLightsCount) {}

						glm::uint32 directionalLightsCount;
					};


				};

			}
		}
	}
}