#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
    namespace PBRenderer {
        namespace Rendering {

            class Scene;

            class RenderingSurface {

                static bool initialize() noexcept;

                static void terminate() noexcept;

            public:
                RenderingSurface(uint32_t width, uint32_t height, std::string windowTitle) noexcept;

                RenderingSurface(const RenderingSurface&) = delete;

                RenderingSurface(RenderingSurface&&) = delete;

                RenderingSurface& operator=(const RenderingSurface&) = delete;

                ~RenderingSurface();

                bool isVulkanSupported() const noexcept;

                GLFWwindow* getWindow() const noexcept;

                GLFWwindow* getWindow() noexcept;

                const std::string& getTitle() const noexcept;

                uint32_t getWidth() const noexcept;

                uint32_t getHeight() const noexcept;

                void onResize(uint32_t newWidth, uint32_t newHeight) noexcept;

                void nextFrame() const noexcept;

                bool shouldClose() const noexcept;

                const VkSurfaceKHR& getSurface() const noexcept;

                template <class T, class ...Args>
                T* createScene(Args ... args) noexcept {
                    T* obj = new T(*this, args...);
                    mScene.reset(obj);

                    return obj;
                }

            private:
                std::string mWindowTitle;

                GLFWwindow* mWindow;

                std::tuple<int, int> mWindowDimensions;

                std::unique_ptr<Scene> mScene;
            };

        }
    }
}
