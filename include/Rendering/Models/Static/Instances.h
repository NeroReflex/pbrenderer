#pragma once

#include "Rendering/SceneElement.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Models {
				namespace Static {

					/**
					 * Represent a collection of instances of some 3D model.
					 *
					 * The used representation is a vector of properties unique to a single instance
					 * (like the model matrix).
					 */
					class Instances :
						virtual public Rendering::SceneElement {

					public:

						static constexpr uint32_t MaxInstancesNumber = 255;

						Instances(Rendering::Scene& parentScene, const std::vector<glm::mat4>& instances = {}) noexcept;

						Instances(const Instances&) = delete;

						Instances(Instances&&) = delete;

						Instances& operator=(const Instances&) = delete;

						~Instances() override;

						VulkanFramework::Buffer* getInstancesBuffer() noexcept;

						uint32_t count() const noexcept;

						const VulkanFramework::Buffer* getInstanceBuffer() const noexcept;

						std::vector<glm::mat4> getInstances() const noexcept;

					private:
						VulkanFramework::Buffer* mInstancesBuffer;

						uint32_t mCount;
					};

				}
			}
		}
	}
}