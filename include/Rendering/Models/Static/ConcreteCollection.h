#pragma once

#include "Rendering/Models/Static/Definition.h"
#include "Rendering/Models/Static/Instances.h"

#include "Loaders/Mesh/MeshLoader.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Models {
				namespace Static {

					/**
					 * Represents a concrete collection of 3D static models.
					 * 
					 * Since a model can be composed of multiple parts with different properties,
					 * a concrete collection contains definition of every such part to draw a complete model
					 * and a buffer of instances, so that multiple instances of a complete model can be drawn.
					 */
					class ConcreteCollection :
						virtual public SceneElement {

					public:
						ConcreteCollection(
							Rendering::Scene& parentScene,
							const std::vector<uintptr_t>& loadedMaterials,
							const Loaders::Mesh::MeshLoader::LoadResult& meshes,
							const std::vector<glm::mat4>& instances = {}
						) noexcept;

						ConcreteCollection(const ConcreteCollection&) = delete;

						ConcreteCollection(ConcreteCollection&&) = delete;

						ConcreteCollection& operator=(const ConcreteCollection&) = delete;

						~ConcreteCollection() override;

						void foreachMesh(std::function<void(const Instances&, const Definition&)> fn) const noexcept;

						Core::AABB getAABB() const noexcept;

					private:
						std::vector<std::unique_ptr<Definition>> mDefinitions;

						std::unique_ptr<Instances> mInstances;
					};

				}
			}
		}
	}
}