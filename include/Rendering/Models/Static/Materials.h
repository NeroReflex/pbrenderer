#pragma once

#include "Rendering/SceneElement.h"

#include "Core/Material.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Models {
				namespace Static {

					class Materials :
						virtual public Rendering::SceneElement {

					public:
						static constexpr uint32_t MaxMaterialsNumber = MAX_MATERIALS;

						Materials(
							Rendering::Scene& parentScene,
							const std::vector<Core::Material>& materials = {}
						) noexcept;

						Materials(const Materials&) = delete;

						Materials(Materials&&) = delete;

						Materials& operator=(const Materials&) = delete;

						~Materials() override;

						const VulkanFramework::Buffer* getMaterialsBuffer() const noexcept;

						void addMaterials(const std::vector<Core::Material>& materials) noexcept;

						uint32_t count() const noexcept;

					private:
						std::vector<Core::Material> mMaterials;

						VulkanFramework::Buffer* const mMaterialsBuffer;
					};

				}
			}
		}
	}
}