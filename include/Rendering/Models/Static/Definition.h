#pragma once

#include "Rendering/SceneElement.h"

#include "Core/Vertex.h"
#include "Core/AABB.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Models {
				namespace Static {

					class Definition :
						virtual public Rendering::SceneElement {

					public:
						Definition(
							Rendering::Scene& parentScene,
							const std::vector<Core::Vertex>& vertexData,
							const std::vector<uint32_t>& vertexIndices,
							const Core::AABB& modelspaceAABB,
							uint32_t materialIndex
						) noexcept;

						Definition(const Definition&) = delete;

						Definition(Definition&&) = delete;

						Definition& operator=(const Definition&) = delete;

						~Definition() override;

						const VulkanFramework::Buffer* getVertexBuffer() const noexcept;

						const VulkanFramework::Buffer* getIndexBuffer() const noexcept;

						uint32_t getVerticesCount() const noexcept;

						const Core::AABB& getModelspaceAABB() const noexcept;

					private:
						VulkanFramework::Buffer* const mVertexBuffer;

						VulkanFramework::Buffer* const mIndexBuffer;

						const uint32_t mVerticesCount;

						const Core::AABB mModelspaceAABB;
					};

				}
			}
		}
	}
}