#pragma once

#include "Rendering/SceneElement.h"

#include "Core/Material.h"

#include "Loaders/Mesh/MeshLoader.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Models {
				namespace Static {

					class Textures :
						virtual public Rendering::SceneElement {

					public:
						static constexpr uint32_t MaxTexturesNumber = MAX_TEXTURE_IN_ARRAY_COUNT;

						static constexpr VulkanFramework::Sampler::Filtering defaultSamplerMagFilter = VulkanFramework::Sampler::Filtering::Nearest;
						static constexpr VulkanFramework::Sampler::Filtering defaultSamplerMinFilter = VulkanFramework::Sampler::Filtering::Nearest;
						static constexpr VulkanFramework::Sampler::MipmapMode defaultSamplerMipMapMode = VulkanFramework::Sampler::MipmapMode::ModeNearest;
						static constexpr float defaultSamplerMaxAnisotropic = 16.0f;

						Textures(
							Rendering::Scene& parentScene
						) noexcept;

						Textures(const Textures&) = delete;

						Textures(Textures&&) = delete;

						Textures& operator=(const Textures&) = delete;

						~Textures() override;

						const std::vector<VulkanFramework::Image*>& getTexturesImage() const noexcept;

						const std::vector<VulkanFramework::ImageView*>& getTexturesImageViews() const noexcept;

						void addTextures(const Core::Texture& texture) noexcept;

						uint32_t count() const noexcept;

						std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*>> getTextures(uint32_t minCount = 0) const noexcept;

						std::vector<std::tuple<VkImageLayout, const VulkanFramework::ImageView*, const VulkanFramework::Sampler*>> getCombinedSamplerTextures(uint32_t minCount = 0) const noexcept;

					private:
						
						VulkanFramework::Image* const mEmptyTextureImage;

						VulkanFramework::ImageView* mEmptyTextureImageView;

						VulkanFramework::Sampler* mDefaultTextureSampler;

						std::vector<VulkanFramework::Image*> mTexturesImages;

						std::vector<VulkanFramework::ImageView*> mTexturesImageViews;

						std::vector<VulkanFramework::Sampler*> mTexturesSamplers;

#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
						static constexpr VkFormat NativeTextureFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
#else
						static constexpr VkFormat NativeTextureFormat = VK_FORMAT_R8G8B8A8_UINT;
#endif
					};

				}
			}
		}
	}
}