#pragma once

#include "PBRenderer.h"

#include "Core/Texture.h"

#include "Instance.h"
#include "Device.h"
#include "Buffer.h"
#include "Framebuffer.h"
#include "Swapchain.h"
#include "Fence.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Rendering {
			namespace Device {

				class RendererDevice {

				public:
					RendererDevice(
						GLFWwindow* window,
						std::unique_ptr<VulkanFramework::Instance> instance,
						const VulkanFramework::Utils::SurfaceDimensions& swapchainInitialDimensions
					) noexcept;

					~RendererDevice();

					RendererDevice(const RendererDevice&) = delete;

					RendererDevice(RendererDevice&&) = delete;

					RendererDevice& operator=(const RendererDevice&) = delete;

					void copyMemoryToBuffer(const void* src, VulkanFramework::Buffer* dst, uint32_t size) const noexcept;

					void copyBufferToMemory(VulkanFramework::Buffer* const src, void* dst, uint32_t size) const noexcept;

					/**
					 * Upload a texture (max staging buffer size) to a vulkan image.
					 *
					 * @param src the source data, tightly packed according to the imageExtent
					 * @param dst the destination image, must be created with the VK_IMAGE_USAGE_TRANSFER_DST_BIT bit set
					 * @param dst dstFinalLayout the final layout
					 */
					void copyMemoryToImage(const void* src, VulkanFramework::Image* const dst, VkImageLayout dstFinalLayout) const noexcept;

					void copyTextureToImage(const Core::Texture& src, VulkanFramework::Image* const dst, VkImageLayout dstFinalLayout) const noexcept;

					void copyTextureMipLevelToImage(const Core::Texture& src, VulkanFramework::Image* const dst, uint32_t mipLevel, VkImageLayout dstFinalLayout) const noexcept;

					VulkanFramework::MemoryPool* getMemoryPoolDeviceOnly() noexcept;

					VulkanFramework::MemoryPool* getMemoryPoolHostCoeherent() noexcept;

					static std::vector<std::string> getRequiredInstanceExtensions();

					static std::vector<std::string> getRequiredDeviceExtensions();

				public:
					std::unique_ptr<VulkanFramework::Instance> g_Instance;

					VkSurfaceKHR g_Surface;

					VulkanFramework::Device* g_Device;

					const VulkanFramework::QueueFamily* g_GraphicsQueueFamily;

					VulkanFramework::Queue* g_GraphicsQueue;

					VulkanFramework::CommandPool* const g_CommandPool;

					VulkanFramework::Swapchain* const g_Swapchain;

				private:
					static VkSurfaceKHR createSurfaceKHR(VulkanFramework::Instance* instance, GLFWwindow* window) noexcept;

					//static constexpr uint32_t stagingBufferSize = 16 * 1024 * 1024; // 16MB
					static constexpr uint32_t stagingBufferSize = 128 * 1024 * 1024; // 128MB for testing

					uint32_t mStagingBufferSize;
					VulkanFramework::Buffer* mStagingBuffer;
					VulkanFramework::CommandBuffer* mStagingBufferCommandBuffer;
					VulkanFramework::Fence* mStagingBufferFence;
					
					VulkanFramework::MemoryPool* mMemoryPoolHostCoherent;
					VulkanFramework::MemoryPool* mMemoryPoolDeviceOnly;
				};

			}
		}
	}
}