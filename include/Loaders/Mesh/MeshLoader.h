#pragma once

#include "PBRenderer.h"

#include "Core/Vertex.h"
#include "Core/Material.h"
#include "Core/AABB.h"
#include "Core/Texture.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Loaders {
			namespace Mesh {

				class MeshLoader {

				public:
					
					class LoadedMtl {
					
					public:
						explicit LoadedMtl(
							const std::string& name = "",
							const glm::vec3& diffuseColor = glm::vec3(0, 0, 0),
							const std::shared_ptr<Core::Texture>& diffuseTexture = std::make_shared<Core::Texture>(
								1, // width
								1, // height
								std::vector<Core::Texture::TextureDataType>({ Core::Texture::TextureDataType(0, 0, 0, 0) }) // only one textel that is transparent so that the material color is shown when no texture is specified
							)
						) noexcept;

						const std::string& getName() const noexcept;

						const glm::vec3& getDiffuseColor() const noexcept;

						Core::Material getNativeMaterial() const noexcept;

						const Core::Texture* getDiffuseTexture() const noexcept;

					private:
						std::string mName;

						glm::vec3 mDiffuseColor;

						std::shared_ptr<Core::Texture> mDiffuseTexture;
					};

					class LoadedMesh {

					public:
						LoadedMesh(const std::shared_ptr<LoadedMtl>& material = nullptr) noexcept;

						const std::vector<uint32_t>& getIndeces() const noexcept;

						const std::vector<Core::Vertex>& getVertexData() const noexcept;

						void push_vertex(const Core::Vertex& vertex) noexcept;

						Core::AABB getAABB() const noexcept;

						const LoadedMtl* getMaterial() const noexcept;

					private:

						glm::vec3 mMaxAABB, mMinAABB;

						std::vector<uint32_t> mIndices;

						std::vector<Core::Vertex> mVertexData;

						std::shared_ptr<LoadedMtl> mMaterial;
					};

					class LoadResult {

					public:
						LoadResult() noexcept;

						void push_model(const std::string& name, std::unique_ptr<LoadedMesh>&& mesh) noexcept;

						const std::unordered_map<std::string, std::unique_ptr<LoadedMesh>>& getMeshes() const noexcept;

						void foreachLoadedMesh(std::function<void(const std::string&, const LoadedMesh&)> fn) const noexcept;

					private:
						std::unordered_map<std::string, std::unique_ptr<LoadedMesh>> mMeshes;

						std::vector<std::string> mErrors;
					};

					class FormatReader {
					public:
						virtual std::unique_ptr<LoadResult> load(const std::string& filename, const std::string& materialBaseDir) noexcept = 0;
					};

					MeshLoader(std::unique_ptr<FormatReader> formatReader) noexcept;

					std::unique_ptr<LoadResult> load(const std::string& filename, const std::string& materialBaseDir) const noexcept;

				private:
					std::unique_ptr<FormatReader> mFormatReader;
				};

			}
		}
	}
}