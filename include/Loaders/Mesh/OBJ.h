#pragma once

#include "Loaders/Mesh/MeshLoader.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Loaders {
			namespace Mesh {

				class OBJ :
					public MeshLoader::FormatReader {

				public:
					std::unique_ptr<MeshLoader::LoadResult> load(const std::string& filename, const std::string& materialBaseDir) noexcept override;

				private:
					
					std::shared_ptr<MeshLoader::LoadedMtl> loadMaterial(
						const std::string& path,
						const std::string& name,
						const glm::vec3& diffuseColor,
						const std::string& diffuseTextureName
					
					) noexcept;

					std::shared_ptr<Core::Texture> loadTexture(const std::string& path, const std::string& name) noexcept;

					std::unordered_map<std::string, std::shared_ptr<MeshLoader::LoadedMtl>> mMaterials;

					std::unordered_map<std::string, std::shared_ptr<Core::Texture>> mTexture;
				};

			}
		}
	}
}