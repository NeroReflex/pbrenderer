#pragma once

// STL streams
#include <iostream>
#include <fstream>
#include <sstream>

// STL data types
#include <functional>
#include <string>
#include <optional>

// STL containers
#include <initializer_list>
#include <unordered_map>
#include <map>
#include <vector>
#include <array>
#include <list>
#include <set>

// STL memory
#include <memory>

// STL algorithms
#include <algorithm>
#include <utility>
#include <limits>

// GLM math library
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

#define GLFW_INCLUDE_VULKAN

// GLFW
#include <GLFW/glfw3.h>

#include "VulkanFramework.h"

#if !defined(NDEBUG)
#include <assert.h>
#define PBRENDERER_DBG_ASSERT(x) assert(x)
#define PBRENDERER_DBG_ONLY(x) x
#else
#define PBRENDERER_DBG_ASSERT(x) /* x */
#define PBRENDERER_DBG_ONLY(x) /* x */
#endif

// Remove macros that generate compile errors
#undef max
#undef min

#define FROM_CPP
#include "../shaders/config.glsl"

