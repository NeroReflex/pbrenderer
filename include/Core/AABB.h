#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Core {

			/**
			 * Represents an AABB.
			 */
			class AABB {

				alignas(sizeof(glm::vec4)) glm::vec4 mMin;

				alignas(sizeof(glm::vec4)) glm::vec4 mMax;

			public:

				AABB(glm::vec3 min, glm::vec3 max) noexcept;

				AABB join(const AABB& aabb) const noexcept;

				AABB transform(const glm::mat4& transformMatrix) const noexcept;

				glm::vec3 getMinimumPoint() const noexcept;

				glm::vec3 getMaximumPoint() const noexcept;
			};
		}
	}
}
