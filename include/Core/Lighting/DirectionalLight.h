#pragma once

#include "PBRenderer.h"

#include "../AABB.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Core {
			namespace Lighting {

				/**
				 * Represents a directional light in the world.
				 */
				class DirectionalLight {

					glm::vec4 mDirection;

					glm::vec4 mAlbedo;

					AABB mInterestedVolume;

				public:
					DirectionalLight(const AABB& interestedVolume, glm::vec3 direction = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 albedo = glm::vec3(1.0f, 1.0f, 1.0f), glm::float32 intensity = 1.0f) noexcept;

					glm::vec3 getDirection() const noexcept;

					glm::vec3 getAlbedo() const noexcept;

					glm::float32 getIntensity() const noexcept;

					AABB getInterestedVolume() const noexcept;
				};

			}
		}
	}
}
