#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
	namespace PBRenderer {
		namespace Core {
			namespace Lighting {

				/**
				 * Represents a point light in the world.
				 */
				class PointLight {

					alignas(sizeof(glm::vec4)) glm::vec4 mPosition;

					alignas(sizeof(glm::vec4)) glm::vec4 mAlbedo;

				public:

					PointLight(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 albedo = glm::vec3(1.0f, 1.0f, 1.0f), glm::float32 intensity = 1.0f) noexcept;

					glm::vec3 getPosition() const noexcept;

					glm::vec3 getAlbedo() const noexcept;

					glm::float32 getIntensity() const noexcept;
				};

			}
		}
	}
}
