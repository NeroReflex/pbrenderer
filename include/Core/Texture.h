#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
    namespace PBRenderer {
        namespace Core {

            /**
             * Represents a texture.
             */
            class Texture {

            public:
#if CONFIG_TEXTURES_R32G32B32A32_FLOAT
                typedef glm::vec<4, glm::float32> TextureDataType;
#else
                typedef glm::vec<4, glm::uint8> TextureDataType;
#endif

            private:
                class MipLevel {

                public:
                    
                    MipLevel(
                        glm::uint width = 0,
                        glm::uint height = 0,
                        const std::vector<TextureDataType>& data = std::vector<TextureDataType>()
                    ) noexcept;

                    MipLevel(
                        glm::uint width,
                        glm::uint height,
                        std::vector<TextureDataType>&& data
                    ) noexcept;

                    MipLevel& operator=(const MipLevel& src) noexcept;

                    const std::vector<TextureDataType>& getData() const noexcept;

                    glm::uint getWidth() const noexcept;

                    glm::uint getHeight() const noexcept;

                private:

                    glm::uint mWidth;

                    glm::uint mHeight;

                    std::vector<TextureDataType> mData;
                };

            public:

                static uint32_t maxMipLevels(uint32_t w, uint32_t h) noexcept;

                Texture() = delete;

                Texture(
                    glm::uint width,
                    glm::uint height,
                    const std::vector<TextureDataType>& data,
                    std::optional<glm::uint> mipLevels = std::optional<glm::uint>()
                ) noexcept;

                Texture(const Texture&) = default;

                Texture& operator=(const Texture&) = default;

                ~Texture() = default;

                uint32_t countLevels() const noexcept;

                const MipLevel& getMipLevel(uint32_t level) const noexcept;

            private:
                std::vector<MipLevel> mMipLevels;
            };

        }
    }
}
