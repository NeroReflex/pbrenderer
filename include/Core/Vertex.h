#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
    namespace PBRenderer {
        namespace Core {

            /**
             * Represents a vertex.
             */
            class Vertex {

                alignas(sizeof(glm::vec4)) glm::vec4 mPosition;

                alignas(sizeof(glm::vec4)) glm::vec4 mNormal;

                alignas(sizeof(glm::vec2)) glm::vec2 mTexturesUV;

                alignas(sizeof(glm::uint32)) glm::uint32 mMaterialIndex;

            public:

                static constexpr uint32_t offsetOfPosition() noexcept { return offsetof(Vertex, mPosition); }
                static constexpr uint32_t offsetOfNormal() noexcept { return offsetof(Vertex, mNormal); }
                static constexpr uint32_t offsetOfTexturesUV() noexcept { return offsetof(Vertex, mTexturesUV); }
                static constexpr uint32_t offsetOfMaterialIndex() noexcept { return offsetof(Vertex, mMaterialIndex); }

                Vertex() noexcept;

                Vertex(
                    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3 normal = glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec2 textures_uv = glm::vec2(0.0f, 0.0f),
                    uint32_t materialIndex = 0
                ) noexcept;

                Vertex(const Vertex&) = default;

                Vertex& operator=(const Vertex&) = default;

                ~Vertex() = default;

                bool operator==(const Vertex&) const noexcept;

                bool operator!=(const Vertex&) const noexcept;

                glm::vec4 getPosition() const noexcept;

                glm::vec4 getNormal() const noexcept;

                glm::vec2 getTextureCoord() const noexcept;

                void setMaterialIndex(uint32_t materialIndex) noexcept;
            };

        }
    }
}
