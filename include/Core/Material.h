#pragma once

#include "PBRenderer.h"

namespace NeroReflex {
    namespace PBRenderer {
        namespace Core {

            /**
             * Represents a material.
             */
            class Material {

                alignas(sizeof(glm::vec4)) glm::vec4 mDiffuseColor;

                alignas(sizeof(glm::uint32)) glm::uint32 mDiffuseTextureIndex;

                alignas(sizeof(glm::uint32)) glm::uint32 mMaterialFlags;
                
            public:
                Material(glm::vec3 diffuseColor = glm::vec3(1.0f, 1.0f, 1.0f)) noexcept;

                Material(const Material&) = default;

                Material& operator=(const Material&) = default;

                ~Material() = default;

                glm::vec3 getDiffuseColor() const noexcept;

                void setDiffuseTextureIndex(uint32_t index) noexcept;

            };

        }
    }
}
